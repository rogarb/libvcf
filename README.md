libvcf: a library to work with vCard files
========================================

Features
---
* Check validity of .vcf files
* Parse vcf files (using vobject crate by default but also supports ical crate)
* Parse abook files (through abook program which needs to be installed on the 
  system)
* Export data to vcard format (.vcf file)
* Look for duplicate/existing entries 
* Automatically merge data/existing vcards
* TODO: ODF export
