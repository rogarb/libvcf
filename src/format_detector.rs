//! Contains the functions used in automatic format detection.
//!
use std::fs::File;
use std::io::prelude::*;
use std::path::Path;

/// List of supported formats.
#[derive(PartialEq, Debug)]
pub enum Format {
    /// Abook adressbook file format
    Abook,
    /// Virtual Contact File (vcf) format
    Vcard,
    /// Other, unsupported format
    Other,
}

/// Detects the format of a given file.
///
/// Warning: this function panics if it is not possible to open or read the file.
///
/// # Examples
///
/// ```
/// use std::fs;
/// use std::fs::File;
/// use std::io::prelude::*;
/// use std::path::PathBuf;
///
/// use libvcf::format_detector::detect_file_format;
/// use libvcf::format_detector::Format;
///
/// // Create a vcf stub file
/// let filename = PathBuf::from("/tmp/libvcf-testfile.vcf");
/// let mut file = File::create(filename.as_path()).unwrap();
/// file.write_all(b"BEGIN:VCARD\r\nFN:test\r\nEND:VCARD\r\n").unwrap();
///
/// // You should ensure that opening and/or reading the file will not yield an error,
/// // otherwise you might get a panic.
/// let format = detect_file_format(filename.as_path());
///
/// // Remove the file from the filesystem
/// fs::remove_file(filename.as_path()).unwrap();
///
/// assert_eq!(format, Format::Vcard); // format == Format::Vcard
/// ```
pub fn detect_file_format(file: &Path) -> Format {
    let mut data = String::new();
    File::open(file)
        .unwrap_or_else(|e| panic!("Unable to open file {}: {e}", file.display()))
        // Don't read the complete file as we are only looking for data in the first
        // few bytes of the file
        .take(64)
        .read_to_string(&mut data)
        .unwrap_or_else(|e| panic!("Unable to read file {}: {e}", file.display()));
    detect_format(&data)
}

/// Detects the format of the input str.
///
/// # Examples
///
/// ```
/// use libvcf::format_detector::detect_format;
/// use libvcf::format_detector::Format;
///
/// let format = detect_format("BEGIN:VCARD");
///
/// assert_eq!(format, Format::Vcard);
///
/// let format = detect_format(
///     "# abook addressbook file\n\n[format]\nprogram=abook\nversion=0.6.1\n\n\n[0]\n"
/// );
///
/// assert_eq!(format, Format::Abook);
///
/// let format = detect_format("Random data input");
///
/// assert_eq!(format, Format::Other);
/// ```
pub fn detect_format(data: &str) -> Format {
    let mut vc = String::from(data);
    vc.truncate(12);
    if vc.contains("BEGIN:VCARD") {
        Format::Vcard
    } else if data.contains("program=abook") {
        Format::Abook
    } else {
        Format::Other
    }
}

#[cfg(test)]
mod tests {
    use super::Format;

    #[test]
    fn detect_format_vcard() {
        let data = "BEGIN:VCARD\nBLABLABLABLA\n";

        assert_eq!(super::detect_format(data), Format::Vcard);
    }

    #[test]
    fn detect_format_abook() {
        let data = "\
# abook addressbook file

[format]
program=abook
version=0.6.1


[0]
name=\"test\"
email=test@test.com
";

        assert_eq!(super::detect_format(data), Format::Abook);
    }

    #[test]
    fn detect_format_other() {
        let data = "RANDOM\nSTUFF\nIN\nDATA\n";

        assert_eq!(super::detect_format(data), Format::Other);
    }
}
