//! This module defines the VCardProperty enum.
//!
use strum_macros::{Display, EnumIter, EnumString, IntoStaticStr};

/// Supported variants of VCaardProperty fields.
///
/// This is used to select the fields to process in the different modes.
///
/// Implementation detail: the order of the variants matter as the enum iterator
/// is used in VCF::merge_all()
#[derive(
    EnumIter, Copy, Clone, Display, EnumString, PartialEq, IntoStaticStr, Debug, Eq, PartialOrd, Ord,
)]
pub enum VCardProperty {
    /// "N" field
    #[strum(serialize = "N")]
    Name,
    /// "FN" field
    #[strum(serialize = "FN")]
    FormattedName,
    /// "TEL" field
    #[strum(serialize = "TEL")]
    Phone,
    /// "EMAIL" field
    #[strum(serialize = "EMAIL")]
    Email,
}

#[cfg(test)]
mod tests {
    use super::VCardProperty;
    use std::str::FromStr;

    #[test]
    fn from_str() {
        let result = VCardProperty::from_str("N");
        let expected = Ok(VCardProperty::Name);

        assert_eq!(result, expected);

        let result = VCardProperty::from_str("NON_EXISTING_FIELD");
        assert!(result.is_err());
    }
}
