//! This module contains debugging tools (for now only one macro).
//!
//!

/// Prints a message, a variable content or a message with a variable content.
///
/// This macro can be safely used all along the code to print debug messages, as it
/// prints something only in debug builds, not in release builds.
#[macro_export]
#[doc(hidden)]
macro_rules! dbg {
    ( $msg: literal ) => {
        #[cfg(debug_assertions)]
        println!($msg);
    };
    ( $e: expr ) => {
        #[cfg(debug_assertions)]
        println!("{}: {:?}", stringify!($e), $e);
    };
    ( $msg: literal, $e: expr ) => {
        #[cfg(debug_assertions)]
        println!("[{}] {}: {:?}", $msg, stringify!($e), $e);
    };
}
