//! This module defines the `Vcard` struct and its methods and traits.
//!
use crate::VCardProperty;
use std::collections::BTreeMap;
use std::str::FromStr;
use vobject::Property;

mod eq;
mod from;
#[macro_use]
mod macros;
mod ordering;

/// The crate's representation of a VCARD entry.
///
/// It is the same representation as the vobject crate. Redefining it locally
/// allows to expand its Trait implementations.
///
/// # Examples
///
/// Let's create a `Vcard` object using the new() function:
/// ```
/// use std::any::{Any, TypeId};
/// use libvcf::Vcard;
///
/// let vcard = Vcard::new();
///
/// assert!(TypeId::of::<Vcard>() == vcard.type_id());
/// ```
///
/// It is also possible to create a `Vcard` using the vcard! macro:
/// ```
/// use std::any::{Any, TypeId};
/// use libvcf::Vcard;
/// use libvcf::vcard;
///
/// let vcard = vcard!(
///     fname: [ "Example Person" ],
///     name: [ "Person;Example;;;" ],
///     tel: [ "0123456789" ],
///     email: [ "test@example.com" ]
/// );
/// assert!(TypeId::of::<Vcard>() == vcard.type_id());
/// ```
#[derive(Clone, Default)]
pub struct Vcard(BTreeMap<String, Vec<Property>>);

// Manual implementation to simplify the representation in failing tests
impl std::fmt::Debug for Vcard {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut data = String::new();
        for prop in get_sorted_values(&self.0) {
            // expect that there is at least one property in the field
            data.push_str(format!("\"{}\" => [ ", prop[0].name).as_str());
            let mut value: Vec<String> = prop.iter().map(|e| e.value_as_string()).collect();
            value.sort();
            for val in value {
                data.push_str(format!("\"{}\", ", val).as_str());
            }
            data.push_str("], ");
        }
        write!(f, "{}", data)
    }
}

impl std::fmt::Display for Vcard {
    /// Formats the Vcard to be properly printed, on screen or in a String.
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut data = String::from("BEGIN:VCARD\r\n");
        for vec in get_sorted_values(&self.0) {
            for prop in vec {
                if let Some(group) = &prop.prop_group {
                    data.push_str(format!("{group}.").as_str());
                }
                data.push_str(&prop.name);
                if !prop.params.is_empty() {
                    for (name, value) in prop.params.iter() {
                        data.push_str(format!(";{name}").as_str());
                        if !value.is_empty() {
                            data.push_str(format!("={value}").as_str());
                        }
                    }
                }
                data.push_str(format!(":{}\r\n", prop.value_as_string()).as_str());
            }
        }
        data.push_str("END:VCARD\r\n");
        write!(f, "{}", data)
    }
}

impl Vcard {
    /// Returns a new `Vcard` using the Default implementation
    pub fn new() -> Self {
        Self::default()
    }

    /// Returns true if at least one field in a Property of self is present in other
    pub fn cmp_properties(&self, other: &Self) -> bool {
        let mut hm = std::collections::HashMap::new();
        self.name_properties()
            .chain(other.name_properties())
            // filter out the VERSION field as it is not relevant in comparison
            .filter(|(n, _)| n.as_str() != "VERSION")
            .for_each(|(_, v)| {
                v.iter().for_each(|prop| {
                    let _ = hm
                        .entry(prop.value_as_string())
                        .and_modify(|e| *e += 1)
                        .or_insert(0);
                })
            });

        hm.values().any(|&v| v != 0)
    }

    /// Returns a list of the properties sharing a common field
    pub fn has_properties_in_common_with(&self, other: &Self) -> Vec<VCardProperty> {
        let mut hm = std::collections::HashMap::new();
        self.name_properties()
            .chain(other.name_properties())
            // filter out the VERSION field as it is not relevant in comparison
            .filter(|(n, _)| n.as_str() != "VERSION")
            .for_each(|(_, v)| {
                v.iter().for_each(|prop| {
                    let _ = hm
                        .entry((prop.name.to_string(), prop.value_as_string()))
                        .and_modify(|e| *e += 1)
                        .or_insert(0);
                })
            });

        let mut properties = Vec::new();
        hm.iter().filter(|(_, &v)| v != 0).for_each(|((n, _), _)| {
            if let Ok(prop) = VCardProperty::from_str(n) {
                properties.push(prop);
            }
        });
        properties
    }

    /// Returns true if at least one Property in self is present in other
    pub fn cmp_property(&self, other: &Self, property: VCardProperty) -> bool {
        match (self.0.get(property.into()), other.0.get(property.into())) {
            (Some(vec1), Some(vec2)) => vec1
                .iter()
                .any(|prop1| vec2.iter().any(|prop2| property_eq(prop1, prop2))),
            (_, _) => false,
        }
    }

    /// Returns a vector containing the raw values of a given property.
    pub fn get_property_values(&self, property: VCardProperty) -> Vec<String> {
        self.0
            .iter()
            .filter(|(name, _)| **name == property.to_string())
            .flat_map(|(_, props)| props.iter().map(|prop| prop.value_as_string()))
            .collect()
    }

    /// Returns an iterator over the field names and the corresponding Vec<Property>.
    pub fn name_properties(&self) -> std::collections::btree_map::Iter<'_, String, Vec<Property>> {
        self.0.iter()
    }
}

/// A hacky way to properly sort a (String, Vec<_>) by mapping a String to a u32.
fn field_priority(s: &str) -> u32 {
    match s {
        "VERSION" => 1,
        "FN" => 2,
        "N" => 3,
        "TEL" => 4,
        "EMAIL" => 5,
        _ => 10,
    }
}

/// Extracts a copy of a [`BTreeMap`]'s values in a [`Vec`] by key using the
/// `field_priority()` helper.
///
/// The returned [`Vec`] is properly ordered for Display.
// HACK: map the String name field to an int depending on its given priority on
// the output, so u32::cmp can be used instead of a custom comparison
// function
fn get_sorted_values<T: Clone>(v: &BTreeMap<String, T>) -> Vec<T> {
    let mut v: Vec<(u32, T)> = v
        .iter()
        .map(|(a, b)| (field_priority(a), b.clone()))
        .collect();
    v.sort_unstable_by(|(a, _), (b, _)| a.cmp(b));
    v.into_iter().map(|(_, b)| b).collect()
}

impl Vcard {
    /// Looks for duplicate vobject::property::Property in self.0.props and drop them.
    fn reduce(self) -> Self {
        let props = self
            .0
            .into_iter()
            .map(|(name, mut vec)| {
                // Vec must be sorted before calling dedup_by()
                vec.sort_unstable_by(property_sorting);
                vec.dedup_by(|a, b| property_eq(a, b));
                (name, vec)
            })
            .collect();
        Self(props)
    }

    /// Keeps a single value for the formatted name field, ditching the other values and
    /// the formatted name ("N") values.
    ///
    /// Warning: this function doesn't ensure the existence of a "FN" field in the
    /// resulting [`Vcard`].
    pub fn keep_single_fname(&mut self) {
        self.keep_single_fn_n_helper();
        // Prefer "FN" entry over "N" entry
        if self.0.contains_key("N") && self.0.contains_key("FN") {
            self.0.remove("N");
        }
    }

    /// Keeps a single value for the name field, ditching the other values and
    /// the formatted name ("FN") values.
    ///
    /// Warning: this function doesn't ensure the existence of a "FN" field in the
    /// resulting [`Vcard`].
    pub fn keep_single_name(&mut self) {
        self.keep_single_fn_n_helper();
        // Prefer "N" entry over "FN" entry
        if self.0.contains_key("N") && self.0.contains_key("FN") {
            self.0.remove("FN");
        }
    }

    /// Keeps only one single value in the "FN" and "N" fields.
    fn keep_single_fn_n_helper(&mut self) {
        self.0.iter_mut().for_each(|(name, vec)| {
            // Keep only one entry for the "N" and "FN" fields
            if name == "N" || name == "FN" {
                // sort the vec before ditching the extra fields
                // use the stable version of sort as we expect a stable ordering
                // and performance should not be too much an issue as there
                // should not be too many values to order
                vec.sort_by_key(|k| k.value_as_string());
                if name == "N" {
                    // for some unknown reason, "N" and "FN" fields are sorted in
                    // opposite orders, this fixes the issue.
                    // TODO: investigate Property::value_as_string()
                    vec.reverse();
                }
                // ditch any extra formatted name/name field
                vec.truncate(1);
            }
        });
    }
}

/// Helper function to order Vec<Property> objects.
fn property_sorting(
    prop1: &vobject::property::Property,
    prop2: &vobject::property::Property,
) -> std::cmp::Ordering {
    prop1
        .prop_group
        .cmp(&prop2.prop_group)
        .then(prop1.name.cmp(&prop2.name))
        .then(prop1.raw_value.cmp(&prop2.raw_value))
        .then(prop1.params.cmp(&prop2.params))
}

/// Helper function to compare Property objects.
///
/// As the code is used in different places, it makes sense to put it in a function.
fn property_eq(prop1: &vobject::property::Property, prop2: &vobject::property::Property) -> bool {
    prop1.name == prop2.name                    // String
        //
        // Ignore the params field when comparing two Property objects.
        // As a consequence, the properties built from EMAIL:foo@bar.com and 
        // EMAIL;INTERNET:foo@bar.com are equal.
        //
        // && prop1.params == prop2.params         // BtreeMap
        && prop1.raw_value == prop2.raw_value   // String
        && prop1.prop_group == prop2.prop_group // Option<String>
}

impl Vcard {
    /// Tests equality of two Vcard objects. /!\ field ordering matters /!\
    pub fn full_eq(&self, other: &Self) -> bool {
        self.0
            .iter()
            .zip(other.0.iter())
            .all(|((prop_name_s, props_s), (prop_name_o, props_o))| {
                prop_name_s == prop_name_o
                    && props_s
                        .iter()
                        .zip(props_o.iter())
                        .all(|(x, y)| property_eq(x, y))
            })
    }
}

impl Vcard {
    /// Tests if the given property contains the given substring
    pub fn contains_property_value(&self, property: &VCardProperty, value_substring: &str) -> bool {
        self.get_property_values(property.to_owned())
            .iter()
            .any(|v| v.contains(value_substring))
    }
}

#[cfg(test)]
mod tests {
    use super::VCardProperty::*;

    // A function to generate vobject::Property objects (without support for groups
    // but with support for field parameters)
    fn property(s: &str) -> vobject::Property {
        let v = s.trim().split(':').collect::<Vec<_>>();
        let (name, raw_value) = match v.len() {
            1 => (String::from(v[0]), String::new()),
            2 => (String::from(v[0]), String::from(v[1])),
            _ => panic!("Invalid input"),
        };
        let v = name.split(';').collect::<Vec<_>>();

        let (name, params) = match v.len() {
            0 => panic!("Invalid input"),
            1 => (String::from(v[0]), std::collections::BTreeMap::new()),
            _ => {
                let mut hm = std::collections::BTreeMap::new();
                for substr in v.iter().skip(1) {
                    let v = substr.split('=').collect::<Vec<_>>();
                    if v.len() == 2 {
                        hm.insert(v[0].to_owned(), v[1].to_owned());
                    }
                }
                (String::from(v[0]), hm)
            }
        };
        let mut property = vobject::Property::new(name, raw_value);
        property.params = params;
        property
    }

    #[test]
    fn vcard_field_order_in_vec() {
        use super::field_priority;

        let mut vec: Vec<(u32, _)> = vec!["TEL", "VERSION", "FN"]
            .iter()
            .map(|&s| (field_priority(s), s))
            .collect();
        vec.sort_unstable_by(|a, b| a.cmp(b));
        let vec: Vec<&str> = vec.iter().map(|(_, s)| *s).collect();

        assert_eq!(vec, vec!["VERSION", "FN", "TEL"]);
    }

    #[test]
    fn get_sorted_values() {
        use super::get_sorted_values;
        use std::collections::BTreeMap;

        let vec = vec!["TEL", "VERSION", "FN"];
        let mut bt = BTreeMap::new();

        for s in vec {
            bt.insert(s.to_string(), s.to_string());
        }

        assert_eq!(get_sorted_values(&bt), vec!["VERSION", "FN", "TEL"]);
    }

    #[test]
    fn reduce() {
        let result = vcard!(
            fname: [ "Test" ],
            name: [ "Test;;;;" ],
            tel: [ "0223456789", "0123456789", "0123456789", "0223456789" ],
            email: [ "email@example.com", "email2@example.com", "email@example.com" ]
        )
        .reduce();
        let expect = vcard!(
            fname: [ "Test" ],
            name: [ "Test;;;;" ],
            tel: [ "0223456789", "0123456789" ],
            email: [ "email@example.com", "email2@example.com" ]
        );

        assert_eq!(result, expect);
    }

    #[test]
    fn keep_single_name_fn_field() {
        let mut result = vcard!(
            fname: [ "Test", "Test2" ],
            tel: [ "0123456789" ],
            email: [ "email@example.com" ]
        );
        result.keep_single_name();
        let expect = vcard!(
            fname: [ "Test" ],
            tel: [ "0123456789" ],
            email: [ "email@example.com" ]
        );

        assert_eq!(result, expect);
    }

    #[test]
    fn keep_single_name_n_field() {
        let mut result = vcard!(
            name: [ "Test;;;;", "Test2;;;;" ],
            tel: [ "0123456789" ],
            email: [ "email@example.com" ]
        );
        result.keep_single_name();
        let expect = vcard!(
            name: [ "Test;;;;" ],
            tel: [ "0123456789" ],
            email: [ "email@example.com" ]
        );

        assert_eq!(result, expect);
    }

    #[test]
    fn keep_single_name_n_and_fn_fields() {
        let mut result = vcard!(
            fname: [ "Test", "Test2" ],
            name: [ "Test;;;;", "Test2;;;;" ],
            tel: [ "0123456789" ],
            email: [ "email@example.com" ]
        );
        result.keep_single_name();
        let expect = vcard!(
            name: [ "Test;;;;" ],
            tel: [ "0123456789" ],
            email: [ "email@example.com" ]
        );

        assert_eq!(result, expect);
    }

    #[test]
    fn equality_for_unordered_vcards() {
        let vc1 = vcard!(
            fname: [ "Test" ],
            tel: [ "0223456789", "0123456789", "0123456789", "0223456789" ],
            email: [ "email@example.com", "email2@example.com", "email@example.com" ]
        );
        let vc2 = vcard!(
            fname: [ "Test" ],
            tel: [ "0223456789", "0223456789", "0123456789", "0123456789" ],
            email: [ "email2@example.com", "email@example.com", "email@example.com" ]
        );

        assert_eq!(vc1, vc2);
        assert!(!vc1.full_eq(&vc2));
    }

    #[test]
    fn cmp_property() {
        // tests for Email
        let vc1 = vcard!(
            fname: [ "Test" ],
            tel: [ "0223456789", "0123456789" ],
            email: [ "email@example.com" ]
        );
        let vc2 = vcard!(
            fname: [ "Test" ],
            tel: [ "0223456789" ],
            email: [ "email2@example.com", "email@example.com" ]
        );
        assert!(vc1.cmp_property(&vc2, Email));

        let vc2 = vcard!(
            fname: [ "Test" ],
            tel: [ "0223456789" ],
            email: [ "email2@example.com" ]
        );
        assert!(!vc1.cmp_property(&vc2, Email));

        // tests for Phone
        let vc1 = vcard!(
            fname: [ "Test" ],
            tel: [ "0223456789", "0123456789" ],
            email: [ "email@example.com" ]
        );
        let vc2 = vcard!(
            fname: [ "Test" ],
            tel: [ "0223456789" ],
            email: [ "email2@example.com", "email@example.com" ]
        );
        assert!(vc1.cmp_property(&vc2, Phone));

        let vc2 = vcard!(
            fname: [ "Test" ],
            tel: [ "02234" ],
            email: [ "email2@example.com" ]
        );
        assert!(!vc1.cmp_property(&vc2, Phone));
    }

    #[test]
    fn cmp_properties() {
        let vc1 = vcard!(
            fname: [ "Test" ],
            tel: [ "0223456789", "0123456789" ],
            email: [ "email@example.com" ]
        );
        let vc2 = vcard!(
            fname: [ "Test2" ],
            tel: [ "0223456789" ],
            email: [ "email2@example.com" ]
        );
        assert!(vc1.cmp_properties(&vc2));

        let vc2 = vcard!(
            fname: [ "Test2" ],
            tel: [ "0323456789" ],
            email: [ "email2@example.com" ]
        );
        assert!(!vc1.cmp_properties(&vc2));

        let vc1 = vcard!(
            fname: [ "Test" ],
            tel: [ "0223456789", "0123456789" ],
            email: [ "email@example.com" ]
        );
        let vc2 = vcard!(
            fname: [ "Test2" ],
            tel: [ "0323456789" ],
            email: [ "email@example.com" ]
        );
        assert!(vc1.cmp_properties(&vc2));
    }

    #[test]
    fn property_eq() {
        let p1 = property("EMAIL:foo@bar.com");
        let p2 = property("EMAIL;TYPE=INTERNET;PREF:foo@bar.com");
        assert!(super::property_eq(&p1, &p2));
        assert_ne!(p1, p2);

        let p1 = property("EMAIL:foo@bar.com");
        let p2 = property("EMAIL:foo@bar.com");
        assert!(super::property_eq(&p1, &p2));
        assert_eq!(p1, p2);

        let p1 = property("EMAIL:foo@bar.com");
        let p2 = property("EMAIL:foo2@bar.com");
        assert!(!super::property_eq(&p1, &p2));
        assert_ne!(p1, p2);
    }

    #[test]
    fn has_properties_in_common_with() {
        let vc1 = vcard!(
            fname: [ "Test" ],
            tel: [ "0223456789", "0123456789" ],
            email: [ "email@example.com" ]
        );
        let vc2 = vcard!(
            fname: [ "Test2" ],
            tel: [ "0323456789" ],
            email: [ "email@example.com" ]
        );
        assert_eq!(vc1.has_properties_in_common_with(&vc2), vec![Email]);

        let vc1 = vcard!(
            fname: [ "Test" ],
            tel: [ "0223456789", "0123456789" ],
            email: [ "email@example.com" ]
        );
        let vc2 = vcard!(
            fname: [ "Test2" ],
            tel: [ "0123456789" ],
            email: [ "email@example.com" ]
        );
        let mut result = vc1.has_properties_in_common_with(&vc2);
        let mut expected = vec![Phone, Email];
        result.sort();
        expected.sort();
        assert_eq!(result, expected);

        let vc1 = vcard!(
            fname: [ "Test" ],
            tel: [ "0223456789", "0123456789" ],
            email: [ "email@example.com" ]
        );
        let vc2 = vcard!(
            fname: [ "Test2" ],
            tel: [ "0323456789" ],
            email: [ "email2@example.com" ]
        );
        assert_eq!(vc1.has_properties_in_common_with(&vc2), vec![]);
    }

    #[test]
    fn display() {
        let result = format!(
            "{}",
            vcard!(
                fname: [ "Test" ],
                tel: [ "0223456789", "0123456789" ],
                email: [ "email@example.com" ]
            )
        );
        let expected = String::from(
            "\
BEGIN:VCARD\r\n\
FN:Test\r\n\
TEL:0223456789\r\n\
TEL:0123456789\r\n\
EMAIL:email@example.com\r\n\
END:VCARD\r\n\
",
        );
        assert_eq!(result, expected);
    }

    #[test]
    fn contains_property_value() {
        let vcard = vcard!(
            fname: [ "Test" ],
            name: [ "Test;;;;" ],
            tel: [ "0223456789", "0123456789", "0123456789", "0223456789" ],
            email: [ "email@example.com", "email2@example.com", "email@example.com" ]
        );

        assert!(vcard.contains_property_value(&crate::vcardproperty::VCardProperty::Name, "Test"));
        assert!(!vcard.contains_property_value(&crate::vcardproperty::VCardProperty::Name, "test"));
        assert!(vcard.contains_property_value(
            &crate::vcardproperty::VCardProperty::Email,
            "email@example.com"
        ));
        assert!(!vcard.contains_property_value(
            &crate::vcardproperty::VCardProperty::Email,
            "invalid@email.com"
        ));
    }
}
