//! Contains the parsing interface, which parses the text data contained in the
//! vcf file, into something which can be transformed in a VCF object.
use crate::vcard::Vcard;
use crate::vcf::split_vcf_str;
#[cfg(feature = "ical")]
use ical::VcardParser;
use std::fs::File;
use std::io::prelude::*;
#[cfg(feature = "ical")]
use std::io::BufReader;
use std::path::PathBuf;
use strum::IntoEnumIterator;
use strum_macros::{Display, EnumIter, EnumString};

mod validator;

/// Parser: contains the data to validate and the associated validator
/// function
pub struct Parser {
    data: String,
    parser_crate: Parsers,
}

/// Supported parsers
#[derive(EnumIter, Debug, Copy, Clone, Display, EnumString)]
#[strum(serialize_all = "snake_case")]
pub enum Parsers {
    /// Parser from ical crate
    #[cfg(feature = "ical")]
    Ical,
    /// Parser from vobject crate
    Vobject,
}

impl Parsers {
    /// Lists the available parsing modes.
    pub fn list_modes() -> Vec<String> {
        let mut modes = Vec::new();
        Parsers::iter().for_each(|mode| modes.push(mode.to_string()));
        modes
    }
}

/// Builder for Parser
///
/// Usage: ParserBuilder::new().add_filename(filename).build()
///        ParserBuilder::new().add_data(data).build()
#[derive(Default)]
pub struct ParserBuilder {
    filename: Option<PathBuf>,
    data: Option<String>,
    parser_crate: Option<Parsers>,
}

impl Default for Parser {
    /// Default implementation for Parser.
    fn default() -> Self {
        Parser {
            data: String::new(),
            // Use the vobject crate by default
            #[cfg(not(feature = "default-parser-ical"))]
            parser_crate: Parsers::Vobject,
            #[cfg(feature = "default-parser-ical")]
            parser_crate: Parsers::Ical,
        }
    }
}

impl ParserBuilder {
    /// Creates a new, empty ParserBuilder.
    pub fn new() -> Self {
        Self::default()
    }

    /// Adds a filename to build the Parser object from.
    ///
    /// This is mutually exclusive with add_data(): panics if self.data.is_some()
    pub fn add_filename(mut self, filename: PathBuf) -> Self {
        assert!(self.data.is_none());
        self.filename = Some(filename);
        self
    }

    /// Adds text content to build the Parser object from.
    ///
    /// This is mutually exclusive with add_filename(): panics if self.filename.is_some()
    pub fn add_data(mut self, data: &str) -> Self {
        assert!(self.filename.is_none());
        self.data = Some(String::from(data));
        self
    }

    /// Specify the validation function to be used
    pub fn parser_crate(mut self, vf: Option<Parsers>) -> Self {
        self.parser_crate = vf;
        self
    }

    /// Build the Parser
    pub fn build(self) -> Parser {
        let mut val = Parser::default();
        if let Some(filename) = self.filename {
            let mut file = File::open(&filename)
                .unwrap_or_else(|_| panic!("Unable to open {}", filename.display()));
            file.read_to_string(&mut val.data)
                .unwrap_or_else(|_| panic!("Unable to read data from {}", filename.display()));
        } else if let Some(data) = self.data {
            val.data = data;
        }
        if let Some(parser_crate) = self.parser_crate {
            val.parser_crate = parser_crate;
        }
        val
    }
}

impl Parser {
    /// Parses the data stored in the parser, yielding the entries as Vcard objects,
    /// silently ignoring parsing error and discarding non valid data.
    pub fn parse(&self) -> Vec<Vcard> {
        match self.try_parse() {
            Ok(vec) => vec,
            Err(_) => Vec::new(),
        }
    }

    /// Tries to parse the data stored in the parser, returning a Result.
    pub fn try_parse(&self) -> Result<Vec<Vcard>, String> {
        match self.parser_crate {
            #[cfg(feature = "ical")]
            Parsers::Ical => try_parse_ical(&self.data),
            Parsers::Vobject => try_parse_vobject(&self.data),
        }
    }
}

/// Associated function to parse the data using the parser from the vobject crate.
fn try_parse_vobject(data: &str) -> Result<Vec<Vcard>, String> {
    // the vobject::Vcard::build() function actually rejects data containing more
    // than one VcARD entry, so the input has to be split into separate strings
    // and each string has to be parsed separately
    split_vcf_str(data)
        .iter()
        .map(|d| {
            vobject::Vcard::build(d)
                .map(Vcard::from)
                .map_err(|e| format!("Vobject parser error: {e}"))
        })
        .collect::<Result<_, _>>()
}

/// Associated function to parse the data using the parser from the ical crate.
#[cfg(feature = "ical")]
fn try_parse_ical(data: &str) -> Result<Vec<Vcard>, String> {
    let buffer = BufReader::new(data.as_bytes());
    // use the vcard parser from the ical crate,
    let contacts = VcardParser::new(buffer);

    // retrieve all the entries in a Vec<VcardContact> (VcardContact is defined
    // in ical crate) which is then mapped to a Vec<Vcard>
    contacts
        .map(|entry| {
            entry
                .map(Vcard::from)
                .map_err(|e| format!("Error parsing .vcf file: {}", e))
        })
        .collect()
}

#[cfg(test)]
mod test {
    use super::ParserBuilder;
    use crate::vcard;

    #[test]
    fn parse() {
        let expected = vec![vcard!(
            fname: [ "FTest" ],
            email: ["test@example.com"],
            tel: ["0123456789"]
        )];
        let result = ParserBuilder::new()
            .add_data(
                format!(
                    "BEGIN:VCARD\r\nFN:FTest\r\nTEL:0123456789\r\nEMAIL:test@example.com\r\nEND:VCARD\r\n"
                )
                .as_str(),
            )
            .build()
            .parse();

        for (vc1, vc2) in expected.iter().zip(result.iter()) {
            assert_eq!(vc1, vc2);
        }
    }

    #[test]
    fn parsed_output_equals_input() {
        let input = "\
BEGIN:VCARD\r\n\
VERSION:3.0\r\n\
FN:Test person\r\n\
N:Person;Test;;;\r\n\
TEL:+30666777888\r\n\
TEL:+34666777888\r\n\
TEL:+33666777888\r\n\
EMAIL;TYPE=INTERNET:alpha_beta@test.com\r\n\
EMAIL;TYPE=INTERNET:alpha_beta3@test.com\r\n\
END:VCARD\r\n\
BEGIN:VCARD\r\n\
VERSION:3.0\r\n\
FN:Test2 TT\r\n\
N:TT;Test2;;;\r\n\
EMAIL;TYPE=INTERNET:example2@test2.com\r\n\
END:VCARD\r\n\
BEGIN:VCARD\r\n\
VERSION:3.0\r\n\
FN:T3\r\n\
N:T;3;;;\r\n\
EMAIL;TYPE=INTERNET:example@test.net\r\n\
END:VCARD\r\n\
"
        .to_string();
        let output: String = ParserBuilder::new()
            .add_data(&input)
            .build()
            .try_parse()
            .expect("BUG: the input should be valid")
            .iter()
            .map(|vc| format!("{vc}"))
            .collect();

        eprintln!("input:\n{}", input);
        eprintln!("output:\n{}", output);
        assert_eq!(input, output);
    }

    #[test]
    #[cfg(not(feature = "default-parser-ical"))]
    fn parse_unicode_input() {
        let input = "\
BEGIN:VCARD\r\n
VERSION:3.0\r\n
FN:灣台水潛\r\n
N:灣台;水潛;;;\r\n
EMAIL;TYPE=INTERNET:example2@test2.com\r\n
END:VCARD\r\n
BEGIN:VCARD\r\n
VERSION:3.0\r\n
FN:ααααα ββββββ\r\n
N:Ββββββ;Αααααα;;;\r\n
TEL;X-Mobile:+30666777888\r\n
EMAIL;TYPE=INTERNET:alpha_beta@test.com\r\n
END:VCARD\r\n
BEGIN:VCARD\r\n
VERSION:3.0\r\n
FN:台旅東遊啟民宿-陳明\r\n
N:;台旅東遊啟民宿-陳明;;;\r\n
EMAIL;TYPE=INTERNET:example@test.net\r\n
END:VCARD\r\n
BEGIN:VCARD\r\n
VERSION:3.0\r\n
FN:ααααα ββββββ\r\n
N:Ββββββ;Αααααα;;;\r\n
TEL;X-Mobile:+30666777888\r\n
EMAIL;TYPE=INTERNET:alpha_beta@test.com\r\n
END:VCARD\r\n
BEGIN:VCARD\r\n
VERSION:3.0\r\n
FN:灣台水潛\r\n
N:灣台;水潛;;;\r\n
EMAIL;TYPE=INTERNET:example2@test2.com\r\n
END:VCARD\r\n";

        let output = ParserBuilder::new().add_data(&input).build().try_parse();

        assert!(output.is_ok());
    }
}
