//! This module contains the From<T> and TryFrom<T> implementations for `VCF`.
//!
use crate::format_detector::{detect_format, Format};
use crate::parser;
use crate::{Vcard, VCF};
use std::fs;
use std::io::prelude::*;
use std::path::{Path, PathBuf};
use std::str::FromStr;

/// Creates a VCF object from a Vec of file paths.
impl TryFrom<Vec<PathBuf>> for VCF {
    type Error = String;
    fn try_from(files: Vec<PathBuf>) -> Result<Self, Self::Error> {
        let mut result = Self::new();
        for file in files {
            let tmp = Self::try_from(file)?;
            result.merge(tmp);
        }
        Ok(result)
    }
}

/// Creates a VCF object from a single file path, which format is automatically detected.
impl TryFrom<PathBuf> for VCF {
    type Error = String;
    fn try_from(filename: PathBuf) -> Result<Self, Self::Error> {
        VCF::try_from(filename.as_path())
    }
}

/// Creates a VCF object from a Path reference.
impl TryFrom<&Path> for VCF {
    type Error = String;
    fn try_from(filename: &Path) -> Result<Self, Self::Error> {
        let mut content = String::new();

        match fs::File::open(filename) {
            Ok(mut file) => {
                if let Err(e) = file.read_to_string(&mut content) {
                    return Err(format!("Unable to read file {}: {}", filename.display(), e));
                }
            }
            Err(e) => return Err(format!("Unable to open file {}: {e}", filename.display())),
        };

        VCF::from_str(&content)
    }
}

/// Creates a VCF object from a single Vcard object (from the vobject crate).
impl From<Vcard> for VCF {
    fn from(vc: Vcard) -> Self {
        Self { entries: vec![vc] }
    }
}

/// Creates a VCF object from a vector containing Vcard objects
impl From<Vec<Vcard>> for VCF {
    fn from(vcards: Vec<Vcard>) -> Self {
        Self { entries: vcards }
    }
}

/// Creates a VCF object from a vector containing VCF objects
impl From<Vec<VCF>> for VCF {
    fn from(vec: Vec<VCF>) -> Self {
        let mut vcf = Self::new();
        vec.into_iter().for_each(|e| vcf.merge(e));
        vcf
    }
}

impl std::str::FromStr for VCF {
    type Err = String;
    /// Creates a VCF object from text content, automatically detecting the format
    fn from_str(content: &str) -> Result<VCF, Self::Err> {
        match detect_format(content) {
            Format::Vcard => parser::ParserBuilder::new()
                .add_data(content)
                .build()
                .try_parse()
                .map(VCF::from),
            Format::Abook => {
                let mut converter = std::process::Command::new("abook")
                    .args(["--convert", "--outformat=vcard"])
                    .stdin(std::process::Stdio::piped())
                    .stdout(std::process::Stdio::piped())
                    .spawn()
                    .map_err(|e| format!("Unable to spawn abook command: {:?}", e))?;
                let mut stdin = converter
                    .stdin
                    .take()
                    .ok_or_else(|| "Unable to take stdin for abook command".to_string())?;
                // TODO: fix lifetime checking
                let stdin_content = String::from(content);
                std::thread::spawn(move || {
                    stdin
                        .write_all(stdin_content.as_bytes())
                        // TODO: handle error
                        .expect("Failed to write to stdin");
                });

                let output = converter
                    .wait_with_output()
                    .map_err(|e| format!("Failed to read stdout for abook: {:?}", e))?;

                if output.status.success() {
                    // Attempt to rewrite the line below in a more idiomatic and correct way
                    // String::from_utf8(output.stdout)
                    //     .map(VCF::from_str)
                    //     .map_err(|e| {
                    //         format!("Unable to convert abook output to UTF-8: {:?}", e)
                    //     })
                    let mut result =
                        VCF::from_str(&String::from_utf8(output.stdout).map_err(|e| {
                            format!("Unable to convert abook output to UTF-8: {:?}", e)
                        })?);
                    result
                        .iter_mut()
                        .for_each(|v| v.entries.iter_mut().for_each(|e| e.keep_single_fname()));
                    result
                } else {
                    Err(format!(
                        "Error parsing abook content: {}",
                        String::from_utf8_lossy(&output.stderr)
                    ))
                }
            }
            Format::Other => Err("Unsupported file format".to_string()),
        }
    }
}

// TODO: write tests for trait implementations
#[cfg(test)]
mod tests {}
