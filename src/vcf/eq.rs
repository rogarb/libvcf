//! This module contains the implementation of the PartialEq trait for `VCF`
//!
use crate::VCF;

impl PartialEq for VCF {
    fn eq(&self, other: &Self) -> bool {
        self.entries().zip(other.entries()).all(|(x, y)| (x == y))
    }
}

#[cfg(test)]
mod tests {
    use crate::vcf;
    use crate::VCF;

    #[test]
    fn eq_empty() {
        let vcf1 = VCF::new();
        let vcf2 = vcf1.clone();

        assert_eq!(vcf1, vcf2);
    }

    #[test]
    fn eq_1_vcard() {
        let vcf1 = vcf!(
            [
                "VERSION": [ "4.0" ],
                "FN": [ "FTest" ],
                "N": [ "Test;;;;" ],
                "TEL": [ "0123456789" ],
                "EMAIL": [ "test@example.com" ]
            ]
        );
        let vcf2 = vcf1.clone();

        assert_eq!(vcf1, vcf2);
    }

    #[test]
    fn eq_3_vcard() {
        let vcf1 = vcf!(
            [
                "VERSION": [ "4.0" ],
                "FN": [ "FTest" ],
                "N": [ "Test;;;;" ],
                "TEL": [ "0123456789" ],
                "EMAIL": [ "test@example.com" ]
            ],
            [
                "VERSION": [ "4.0" ],
                "FN": [ "FTest2" ],
                "N": [ "Test2;;;;" ],
                "TEL": [ "0223456789" ],
                "EMAIL": [ "test2@example.com" ]
            ],
            [
                "VERSION": [ "4.0" ],
                "FN": [ "FTest3" ],
                "N": [ "Test3:q3;;;;" ],
                "TEL": [ "0323456789" ],
                "EMAIL": [ "test3:q3@example.com" ]
            ]
        );
        let vcf2 = vcf1.clone();

        assert_eq!(vcf1, vcf2);
    }

    #[test]
    fn eq_different_input() {
        let vcf1 = vcf!(
            [
                "VERSION": [ "4.0" ],
                "FN": [ "FTest3" ],
                "N": [ "Test3:q3;;;;" ],
                "TEL": [ "0323456789" ],
                "EMAIL": [ "test3:q3@example.com" ],
                "REV": [ "20220605T134526Z" ]
            ]
        );
        let vcf2 = vcf!(
            [
                "VERSION": [ "4.0" ],
                "FN": [ "FTest2" ],
                "N": [ "Test2;;;;" ],
                "TEL": [ "0223456789" ],
                "EMAIL": [ "test2@example.com" ],
                "REV": [ "20220605T134526Z" ]
            ]
        );

        assert_ne!(vcf1, vcf2);
    }
}
