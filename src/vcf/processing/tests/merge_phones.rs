//! Test module containing tests for phone related merge.
//!
use crate::vcf;
use crate::VCardProperty;

#[test]
fn merge_from_property_phone_3entries_2dup() {
    let result = vcf!(
        [
            "FN": ["Test"],
            "N": ["Test;;;;"],
            "TEL": ["0123456789"],
            "EMAIL": ["test@example.com", "test2@example.com"]
        ],
        [
            "FN": ["Test2"],
            "N": ["Test2;;;;"],
            "TEL": ["0123456789", "0213456789"],
            "EMAIL": ["test@test.com", "test2@test.com", "test3@test.com"]
        ],
        [
            "FN": ["Test3"],
            "N": ["Test3;;;;"],
            "TEL": ["0213456789"],
            "EMAIL": ["test@example.com", "test3@example.com"]
        ]
    )
    .merge_from_property(VCardProperty::Phone);

    let expected = vcf!(
        [
            "N": ["Test;;;;"],
            "TEL": ["0123456789", "0213456789"],
            "EMAIL": ["test@test.com", "test2@test.com", "test3@test.com", "test@example.com", "test2@example.com", "test3@example.com"]
        ]
    );
    assert_eq!(result, expected);
}

#[test]
fn merge_from_property_phone_4entries_1dup() {
    let result = vcf!(
        [
            "FN": ["Person"],
            "N": ["Person;;;;"],
            "TEL": ["0323456789"],
            "EMAIL": ["test@domain.com", "test2@domain.com"]
        ],
        [
            "FN": ["Test"],
            "N": ["Test;;;;"],
            "TEL": ["0423456789"],
            "EMAIL": ["test@example.com", "test2@example.com"]
        ],
        [
            "FN": ["Test2"],
            "N": ["Test2;;;;"],
            "TEL": ["0123456789", "0213456789"],
            "EMAIL": ["test@test.com", "test2@test.com", "test3@test.com"]
        ],
        [
            "FN": ["Test3"],
            "N": ["Test3;;;;"],
            "TEL": ["0213456789"],
            "EMAIL": ["test@example.com", "test3@example.com"]
        ]
    )
    .merge_from_property(VCardProperty::Phone);

    let expected = vcf!(
        [
            "N": ["Person;;;;"],
            "TEL": ["0323456789"],
            "EMAIL": ["test@domain.com", "test2@domain.com"]
        ],
        [
            "N": ["Test;;;;"],
            "TEL": ["0423456789"],
            "EMAIL": ["test@example.com", "test2@example.com"]
        ],
        [
            "N": ["Test3;;;;"],
            "TEL": ["0123456789", "0213456789"],
            "EMAIL": ["test@test.com", "test2@test.com", "test3@test.com", "test@example.com", "test3@example.com"]
        ]
    );
    assert_eq!(result, expected);
}
