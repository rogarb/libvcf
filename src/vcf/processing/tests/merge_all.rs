#[test]
fn merge_all_ascii() {
    let result = vcf!(
        [
            "VERSION": [ "3.0" ],
            "FN": [ "Test person" ],
            "N": [ "Person;Test;;;" ],
            "TEL": [ "+30666777888" ],
            "EMAIL;TYPE=INTERNET": [ "alpha_beta@test.com" ]
        ],
        [
            "VERSION": [ "3.0" ],
            "FN": [ "Test2 TT" ],
            "N": [ "TT;Test2;;;" ],
            "EMAIL;TYPE=INTERNET": [ "example2@test2.com" ]
        ],
        [
            "VERSION": [ "3.0" ],
            "FN": [ "Test person" ],
            "N": [ "P;Test;;;" ],
            "TEL": [ "+33666777888" ],
            "EMAIL;TYPE=INTERNET": [ "alpha_beta3@test.com" ]
        ],
        [
            "VERSION": [ "3.0" ],
            "FN": [ "T3" ],
            "N": [ "T;3;;;" ],
            "EMAIL;TYPE=INTERNET": [ "example@test.net" ]
        ],
        [
            "VERSION": [ "3.0" ],
            "FN": [ "Test person" ],
            "N": [ "Person;Test;;;" ],
            "TEL": [ "+34666777888" ],
            "EMAIL;TYPE=INTERNET": [ "alpha_beta@test.com" ]
        ],
        [
            "VERSION": [ "3.0" ],
            "FN": [ "Test2 TT" ],
            "N": [ "TT;Test2;;;" ],
            "EMAIL;TYPE=INTERNET": [ "example2@test2.com" ]
        ]
    )
    .merge_all();
    let expected = vcf!(
        [
            "VERSION": [ "3.0" ],
            "FN": [ "Test2 TT" ],
            "N": [ "TT;Test2;;;" ],
            "EMAIL;TYPE=INTERNET": [ "example2@test2.com" ]
        ],
        [
            "VERSION": [ "3.0" ],
            "FN": [ "T3" ],
            "N": [ "T;3;;;" ],
            "EMAIL;TYPE=INTERNET": [ "example@test.net" ]
        ],
        [
            "VERSION": [ "3.0" ],
            "FN": [ "Test person" ],
            "N": [ "P;Test;;;", "Person;Test;;;" ],
            "TEL": [ "+30666777888" ],
            "TEL": [ "+33666777888" ],
            "TEL": [ "+34666777888" ],
            "EMAIL;TYPE=INTERNET": [ "alpha_beta@test.com" ],
            "EMAIL;TYPE=INTERNET": [ "alpha_beta3@test.com" ]
        ]
    );
    assert_eq!(result, expected);
}

#[test]
// This test is not activated when using the ical parser as a default because
// a subfield of the TEL field is not accepted by the parser.
#[cfg(not(feature = "default-parser-ical"))]
fn merge_all_unicode() {
    let result = vcf!(
        [
            "VERSION": [ "3.0" ],
            "FN": [ "ααααα ββββββ" ],
            "N": [ "Ββββββ;Αααααα;;;" ],
            "TEL;X-Mobile": [ "+30666777888" ],
            "EMAIL;TYPE=INTERNET": [ "alpha_beta@test.com" ]
        ],
        [
            "VERSION": [ "3.0" ],
            "FN": [ "灣台水潛" ],
            "N": [ "灣台;水潛;;;" ],
            "EMAIL;TYPE=INTERNET": [ "example2@test2.com" ]
        ],
        [
            "VERSION": [ "3.0" ],
            "FN": [ "ααααα ββββββ" ],
            "N": [ "Β;Αααααα;;;" ],
            // The X-Mobile subtype makes the ical parser throw an error
            "TEL;X-Mobile": [ "+33666777888" ],
            "EMAIL;TYPE=INTERNET": [ "alpha_beta3@test.com" ]
        ],
        [
            "VERSION": [ "3.0" ],
            "FN": [ "台旅東遊啟民宿-陳明" ],
            "N": [ ";台旅東遊啟民宿-陳明;;;" ],
            "EMAIL;TYPE=INTERNET": [ "example@test.net" ]
        ],
        [
            "VERSION": [ "3.0" ],
            "FN": [ "ααααα ββββββ" ],
            "N": [ "Ββββββ;Αααααα;;;" ],
            "TEL;X-Mobile": [ "+34666777888" ],
            "EMAIL;TYPE=INTERNET": [ "alpha_beta@test.com" ]
        ],
        [
            "VERSION": [ "3.0" ],
            "FN": [ "灣台水潛" ],
            "N": [ "灣台;水潛;;;" ],
            "EMAIL;TYPE=INTERNET": [ "example2@test2.com" ]
        ]
    )
    .merge_all();
    let expected = vcf!(
        [
            "VERSION": [ "3.0" ],
            "FN": [ "灣台水潛" ],
            "N": [ "灣台;水潛;;;" ],
            "EMAIL;TYPE=INTERNET": [ "example2@test2.com" ]
        ],
        [
            "VERSION": [ "3.0" ],
            "FN": [ "台旅東遊啟民宿-陳明" ],
            "N": [ ";台旅東遊啟民宿-陳明;;;" ],
            "EMAIL;TYPE=INTERNET": [ "example@test.net" ]
        ],
        [
            "VERSION": [ "3.0" ],
            "FN": [ "ααααα ββββββ" ],
            "N": [ "Β;Αααααα;;;", "Ββββββ;Αααααα;;;" ],
            "TEL;X-Mobile": [ "+30666777888" ],
            "TEL;X-Mobile": [ "+34666777888" ],
            "TEL;X-Mobile": [ "+33666777888" ],
            "EMAIL;TYPE=INTERNET": [ "alpha_beta@test.com" ],
            "EMAIL;TYPE=INTERNET": [ "alpha_beta3@test.com" ]
        ]
    );
    assert_eq!(result, expected);
}

#[test]
fn merge_all_duplicate_fields_in_vcf() {
    let result = vcf!(
        [
            "VERSION": [ "3.0" ],
            "N": [ "Person;Test;;;" ],
            "TEL": [ "+30666777888" ],
            "EMAIL;TYPE=INTERNET": [ "alpha_beta@test.com" ]
        ],
        [
            "VERSION": [ "3.0" ],
            "FN": [ "Test person" ],
            "TEL": [ "+30666777888" ],
            "EMAIL;TYPE=INTERNET": [ "alpha_beta3@test.com" ]
        ],
        [
            "VERSION": [ "3.0" ],
            "N": [ "P;Test;;;" ],
            "TEL": [ "+34666777888" ],
            "EMAIL;TYPE=INTERNET": [ "alpha_beta@test.com" ]
        ],
        [
            "VERSION": [ "3.0" ],
            "FN": [ "Test person" ],
            "EMAIL;TYPE=INTERNET": [ "example2@test2.com" ]
        ]
    )
    .merge_all();
    let expected = vcf!(
        [
            "VERSION": [ "3.0" ],
            "FN": [ "Test person" ],
            "N": [ "P;Test;;;", "Person;Test;;;" ],
            "TEL": [ "+30666777888", "+34666777888" ],
            "EMAIL;TYPE=INTERNET": [ "alpha_beta@test.com" ],
            "EMAIL;TYPE=INTERNET": [ "alpha_beta3@test.com" ],
            "EMAIL;TYPE=INTERNET": [ "example2@test2.com" ]
        ]
    );
    assert_eq!(result, expected);
}
