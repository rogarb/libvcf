//! Test module containing unique entries removal tests
//!
use crate::vcf;

#[test]
// This test is not activated when using the ical parser as a default because
// a subfield of the TEL field is not accepted by the parser.
#[cfg(not(feature = "default-parser-ical"))]
fn get_unique_entries_in_vcf() {
    let result = vcf!(
        [
            "VERSION": [ "3.0" ],
            "FN": [ "ααααα ββββββ" ],
            "N": [ "Ββββββ;Αααααα;;;" ],
            // The X-Mobile subtype makes the ical parser throw an error
            "TEL;X-Mobile": [ "+30666777888" ],
            "EMAIL;TYPE=INTERNET": [ "alpha_beta@test.com" ]
        ],
        [
            "VERSION": [ "3.0" ],
            "FN": [ "灣台水潛" ],
            "N": [ "灣台;水潛;;;" ],
            "EMAIL;TYPE=INTERNET": [ "example2@test2.com" ]
        ],
        [
            "VERSION": [ "3.0" ],
            "FN": [ "ααααα ββββββ" ],
            "N": [ "Ββββββ;Αααααα;;;" ],
            "TEL;X-Mobile": [ "+30666777888" ],
            "EMAIL;TYPE=INTERNET": [ "alpha_beta@test.com" ]
        ],
        [
            "VERSION": [ "3.0" ],
            "FN": [ "台旅東遊啟民宿-陳明" ],
            "N": [ ";台旅東遊啟民宿-陳明;;;" ],
            "EMAIL;TYPE=INTERNET": [ "example@test.net" ]
        ],
        [
            "VERSION": [ "3.0" ],
            "FN": [ "ααααα ββββββ" ],
            "N": [ "Ββββββ;Αααααα;;;" ],
            "TEL;X-Mobile": [ "+30666777888" ],
            "EMAIL;TYPE=INTERNET": [ "alpha_beta@test.com" ]
        ],
        [
            "VERSION": [ "3.0" ],
            "FN": [ "灣台水潛" ],
            "N": [ "灣台;水潛;;;" ],
            "EMAIL;TYPE=INTERNET": [ "example2@test2.com" ]
        ]
    )
    .get_unique_entries();
    let expected = vcf!(
        [
            "VERSION": [ "3.0" ],
            "FN": [ "ααααα ββββββ" ],
            "N": [ "Ββββββ;Αααααα;;;" ],
            "TEL;X-Mobile": [ "+30666777888" ],
            "EMAIL;TYPE=INTERNET": [ "alpha_beta@test.com" ]
        ],
        [
            "VERSION": [ "3.0" ],
            "FN": [ "灣台水潛" ],
            "N": [ "灣台;水潛;;;" ],
            "EMAIL;TYPE=INTERNET": [ "example2@test2.com" ]
        ],
        [
            "VERSION": [ "3.0" ],
            "FN": [ "台旅東遊啟民宿-陳明" ],
            "N": [ ";台旅東遊啟民宿-陳明;;;" ],
            "EMAIL;TYPE=INTERNET": [ "example@test.net" ]
        ]
    );
    assert_eq!(result, expected);
}

#[test]
fn get_unique_entries_in_abook() {
    use std::str::FromStr;
    let mut result = crate::VCF::new();
    result.merge(
        crate::VCF::from_str(
            "\
# abook addressbook file

[format]
program=abook
version=0.6.1


[0]
name=Test1
email=test@test.com,romain@aferzz.fr,frezfrez@frezfrz.fr
phone=03425464
workphone=85764534234354
fax=87564325346576
mobile=0145325462


[1]
name=Test1
email=test@test.com,romain@aferzz.fr,frezfrez@frezfrz.fr
phone=03425464
workphone=85764534234354
fax=87564325346576
mobile=0145325462


[2]
name=Test2
email=test@test.com,romain@aferzz.fr,frezfrez@frezfrz.fr
phone=03425464
workphone=85764534234354
fax=87564325346576
mobile=0145325462
",
        )
        .expect("Test input shouldn't cause an error"),
    );
    let result = result.get_unique_entries();
    let expected = vcf!(
        [
            "FN": [ "Test1" ],
            "TEL": [ "03425464", "85764534234354", "87564325346576", "0145325462" ],
            "EMAIL": [ "test@test.com", "romain@aferzz.fr", "frezfrez@frezfrz.fr" ]
        ],
        [
            "FN": [ "Test2" ],
            "TEL": [ "03425464", "85764534234354", "87564325346576", "0145325462" ],
            "EMAIL": [ "test@test.com", "romain@aferzz.fr", "frezfrez@frezfrz.fr" ]
        ]
    );
    assert_eq!(result, expected);
}

#[test]
fn get_unique_entries_in_abook_and_vcf() {
    use std::str::FromStr;
    let mut abook = crate::VCF::new();
    abook.merge(
        crate::VCF::from_str(
            "\
# abook addressbook file

[format]
program=abook
version=0.6.1


[0]
name=Test1
email=test@test.com,romain@aferzz.fr,frezfrez@frezfrz.fr
phone=03425464
workphone=85764534234354
fax=87564325346576
mobile=0145325462


[1]
name=Test1
email=test@test.com,romain@aferzz.fr,frezfrez@frezfrz.fr
phone=03425464
workphone=85764534234354
fax=87564325346576
mobile=0145325462


[2]
name=Test2
email=test@test.com,romain@aferzz.fr,frezfrez@frezfrz.fr
phone=03425464
workphone=85764534234354
fax=87564325346576
mobile=0145325462
",
        )
        .expect("Test input shouldn't cause an error"),
    );
    abook.merge(vcf!(
        [
            "FN": [ "Test1" ],
            "TEL": [ "03425464", "85764534234354", "87564325346576", "0145325462" ],
            "EMAIL": [ "test@test.com", "romain@aferzz.fr", "frezfrez@frezfrz.fr" ]
        ]
    ));
    let result = abook.get_unique_entries();
    let expected = vcf!(
        [
            "FN": [ "Test1" ],
            "TEL": [ "03425464", "85764534234354", "87564325346576", "0145325462" ],
            "EMAIL": [ "test@test.com", "romain@aferzz.fr", "frezfrez@frezfrz.fr" ]
        ],
        [
            "FN": [ "Test2" ],
            "TEL": [ "03425464", "85764534234354", "87564325346576", "0145325462" ],
            "EMAIL": [ "test@test.com", "romain@aferzz.fr", "frezfrez@frezfrz.fr" ]
        ]
    );
    assert_eq!(result, expected);
}
