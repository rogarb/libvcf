//! Test module containing tests for name related merge.
//!
use crate::vcf;
use crate::VCardProperty;

#[test]
fn merge_from_property_name_3entries_nodup() {
    let result = vcf!(
        [
            "FN": ["Test"],
            "N": ["Test;;;;"],
            "TEL": ["0123456789"],
            "EMAIL": ["test@example.com", "test2@example.com"]
        ],
        [
            "FN": ["Test"],
            "N": ["Test2;;;;"],
            "TEL": ["0123456789", "0213456789"],
            "EMAIL": ["test@test.com", "test2@test.com", "test3@test.com"]
        ],
        [
            "FN": ["Test3"],
            "N": ["Test3;;;;"],
            "TEL": ["0213456789"],
            "EMAIL": ["test@example.com", "test3@example.com"]
        ]
    )
    .merge_from_property(VCardProperty::Name);

    let expected = vcf!(
        // the VCF object is now always reduced to one entry for N
        [
            "N": ["Test;;;;"],
            "TEL": ["0123456789"],
            "EMAIL": ["test@example.com", "test2@example.com"]
        ],
        [
            "N": ["Test2;;;;"],
            "TEL": ["0123456789", "0213456789"],
            "EMAIL": ["test@test.com", "test2@test.com", "test3@test.com"]
        ],
        [
            "N": ["Test3;;;;"],
            "TEL": ["0213456789"],
            "EMAIL": ["test@example.com", "test3@example.com"]
        ]
    );
    assert_eq!(result, expected);
}

#[test]
fn merge_from_property_name_4entries_1dup() {
    let result = vcf!(
        [
            "FN": ["Person"],
            "N": ["Person;;;;"],
            "TEL": ["0123456789"],
            "EMAIL": ["test@domain.com", "test2@domain.com"]
        ],
        [
            "FN": ["Test"],
            "N": ["Test;;;;"],
            "TEL": ["0123456789"],
            "EMAIL": ["test@example.com", "test2@example.com"]
        ],
        [
            "FN": ["Test2"],
            "N": ["Test2;;;;"],
            "TEL": ["0123456789", "0213456789"],
            "EMAIL": ["test@test.com", "test2@test.com", "test3@test.com"]
        ],
        [
            "FN": ["Test3"],
            "N": ["Test;;;;"],
            "TEL": ["0213456789"],
            "EMAIL": ["test@example.com", "test3@example.com"]
        ]
    )
    .merge_from_property(VCardProperty::Name);

    let expected = vcf!(
        [
            "N": ["Person;;;;"],
            "TEL": ["0123456789"],
            "EMAIL": ["test@domain.com", "test2@domain.com"]
        ],
        [
            "N": ["Test2;;;;"],
            "TEL": ["0123456789", "0213456789"],
            "EMAIL": ["test@test.com", "test2@test.com", "test3@test.com"]
        ],
        [
            "N": ["Test;;;;"],
            "TEL": ["0123456789", "0213456789"],
            "EMAIL": ["test@example.com", "test2@example.com", "test3@example.com"]
        ]
    );
    assert_eq!(result, expected);
}
