//! Test module containing unique entries removal tests
//!
use crate::vcf;

#[test]
fn filter_vcf() {
    let result = vcf!(
        [
            "VERSION": [ "3.0" ],
            "FN": [ "ααααα ββββββ" ],
            "N": [ "Ββββββ;Αααααα;;;" ],
            // The X-Mobile subtype makes the ical parser throw an error
            "TEL;X-Mobile": [ "+30666777888" ],
            "EMAIL;TYPE=INTERNET": [ "alpha_beta@test.com" ]
        ],
        [
            "VERSION": [ "3.0" ],
            "FN": [ "灣台水潛" ],
            "N": [ "灣台;水潛;;;" ],
            "EMAIL;TYPE=INTERNET": [ "example2@test2.com" ]
        ],
        [
            "VERSION": [ "3.0" ],
            "FN": [ "ααααα ββββββ" ],
            "N": [ "Ββββββ;Αααααα;;;" ],
            "TEL;X-Mobile": [ "+30666777888" ],
            "EMAIL;TYPE=INTERNET": [ "alpha_beta@test.com" ]
        ],
        [
            "VERSION": [ "3.0" ],
            "FN": [ "台旅東遊啟民宿-陳明" ],
            "N": [ ";台旅東遊啟民宿-陳明;;;" ],
            "EMAIL;TYPE=INTERNET": [ "example@test.net" ]
        ],
        [
            "VERSION": [ "3.0" ],
            "FN": [ "ααααα ββββββ" ],
            "N": [ "Ββββββ;Αααααα;;;" ],
            "TEL;X-Mobile": [ "+30666777888" ],
            "EMAIL;TYPE=INTERNET": [ "alpha_beta@test.com" ]
        ],
        [
            "VERSION": [ "3.0" ],
            "FN": [ "灣台水潛" ],
            "N": [ "灣台;水潛;;;" ],
            "EMAIL;TYPE=INTERNET": [ "example2@test2.com" ]
        ]
    )
    .filter_by_property_value(&crate::vcardproperty::VCardProperty::Email, "example");
    let expected = vcf!(
        [
            "VERSION": [ "3.0" ],
            "FN": [ "灣台水潛" ],
            "N": [ "灣台;水潛;;;" ],
            "EMAIL;TYPE=INTERNET": [ "example2@test2.com" ]
        ],
        [
            "VERSION": [ "3.0" ],
            "FN": [ "台旅東遊啟民宿-陳明" ],
            "N": [ ";台旅東遊啟民宿-陳明;;;" ],
            "EMAIL;TYPE=INTERNET": [ "example@test.net" ]
        ],
        [
            "VERSION": [ "3.0" ],
            "FN": [ "灣台水潛" ],
            "N": [ "灣台;水潛;;;" ],
            "EMAIL;TYPE=INTERNET": [ "example2@test2.com" ]
        ]

    );
    assert_eq!(result, expected);
}
