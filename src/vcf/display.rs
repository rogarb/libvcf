//! This module implements the `Display` trait for `VCF`.
//!
use crate::VCF;
use std::fmt;
use std::fmt::Display;

impl Display for VCF {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut data = String::new();
        for entry in self.entries.iter() {
            data.push_str(format!("{}", entry).as_str());
        }
        write!(f, "{}", data)
    }
}

// Manual implementation to simplify the representation in failing tests
impl std::fmt::Debug for VCF {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut data = String::from("VCF: entries:\n");
        for entry in self.entries() {
            data.push_str(format!("[ {:?} ]\n", entry).as_str());
        }
        write!(f, "{}", data)
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn display() {
        let result = format!(
            "{}",
            vcf!(
                [
                    "N": [ "Test2;;;;" ],
                    "TEL;X-Mobile": [ "0213456789" ],
                    "FN": [ "Test2" ],
                    "EMAIL;TYPE=INTERNET": [ "foo2@bar.com" ]
                ],
                [
                    "N": [ "Test;;;;" ],
                    "TEL;X-Mobile": [ "0123456789" ],
                    "FN": [ "Test" ],
                    "EMAIL;TYPE=INTERNET": [ "foo@bar.com" ]
                ]
            )
        );
        let expected = String::from(
            "\
BEGIN:VCARD\r\n\
FN:Test\r\n\
N:Test;;;;\r\n\
TEL;X-Mobile:0123456789\r\n\
EMAIL;TYPE=INTERNET:foo@bar.com\r\n\
END:VCARD\r\n\
BEGIN:VCARD\r\n\
FN:Test2\r\n\
N:Test2;;;;\r\n\
TEL;X-Mobile:0213456789\r\n\
EMAIL;TYPE=INTERNET:foo2@bar.com\r\n\
END:VCARD\r\n\
",
        );

        assert_eq!(result, expected);
    }
}
