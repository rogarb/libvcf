//! Contains various statistics related methods for `VCF`
//!
use crate::VCardProperty;
use crate::VCF;
use std::collections::HashMap;

impl VCF {
    /// Counts the duplicates in a VCF object given a VCardProperty.
    pub fn count_property_duplicates(&self, property: VCardProperty) -> usize {
        let mut hm = HashMap::new();
        self.entries()
            .flat_map(|vcard| vcard.get_property_values(property))
            .for_each(|val| {
                let _ = hm.entry(val).and_modify(|e| *e += 1).or_insert(0);
            });

        hm.values().filter(|&&val| val != 0).count()
    }
}

#[cfg(test)]
mod tests {
    use crate::vcf;
    use crate::VCardProperty;

    #[test]
    fn count_email_duplicates_no_dups() {
        let result = vcf!(
            [
                "FN": ["Test"],
                "N": ["Test;;;;"],
                "TEL": ["0123456789"],
                "EMAIL": ["test@example.com", "test2@example.com"]
            ],
            [
                "FN": ["Test2"],
                "N": ["Test2;;;;"],
                "TEL": ["0123456789", "0213456789"],
                "EMAIL": ["test@test.com", "test2@test.com", "test3@test.com"]
            ],
            [
                "FN": ["Test3"],
                "N": ["Test3;;;;"],
                "TEL": ["0213456789"],
                "EMAIL": ["test3@example.com"]
            ]
        )
        .count_property_duplicates(VCardProperty::Email);

        let expected = 0;
        assert_eq!(result, expected);
    }

    #[test]
    fn count_email_duplicates_1dup() {
        let result = vcf!(
            [
                "FN": ["Test"],
                "N": ["Test;;;;"],
                "TEL": ["0123456789"],
                "EMAIL": ["test@example.com", "test2@example.com"]
            ],
            [
                "FN": ["Test2"],
                "N": ["Test2;;;;"],
                "TEL": ["0123456789", "0213456789"],
                "EMAIL": ["test@test.com", "test2@test.com", "test3@test.com"]
            ],
            [
                "FN": ["Test3"],
                "N": ["Test3;;;;"],
                "TEL": ["0213456789"],
                "EMAIL": ["test@example.com", "test3@example.com"]
            ]
        )
        .count_property_duplicates(VCardProperty::Email);

        let expected = 1;
        assert_eq!(result, expected);
    }

    #[test]
    fn count_email_duplicates_3dups() {
        let result = vcf!(
            [
                "FN": ["Test"],
                "N": ["Test;;;;"],
                "TEL": ["0123456789"],
                "EMAIL": ["test@example.com", "test2@example.com"]
            ],
            [
                "FN": ["Test2"],
                "N": ["Test2;;;;"],
                "TEL": ["0123456789", "0213456789"],
                "EMAIL": ["test@example.com", "test2@example.com", "test3@example.com"]
            ],
            [
                "FN": ["Test3"],
                "N": ["Test3;;;;"],
                "TEL": ["0213456789"],
                "EMAIL": ["test@example.com", "test3@example.com"]
            ]
        )
        .count_property_duplicates(VCardProperty::Email);

        let expected = 3;
        assert_eq!(result, expected);
    }
}
