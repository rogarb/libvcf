//! This module contains the methods used for processing the entries in the `VCF`.
//!
use crate::VCF;
use crate::{VCardProperty, Vcard};

impl VCF {
    /// Return a VCF object containing only the unique VCard entries in self
    ///
    /// This uses the PartialEq implementation from the vcard crate.
    pub fn get_unique_entries(mut self) -> VCF {
        let mut unique_vcf = VCF::new();
        while let Some(entry) = self.entries.pop() {
            // Entry is kept if it doesn't exist in the new set
            // The any() method is used as it doesn't need to compare all the set
            if !unique_vcf.entries.iter().any(|e| *e == entry) {
                unique_vcf.add_entry(entry);
            }
        }
        unique_vcf.sort();
        unique_vcf
    }

    /// Pop the entries which contain a duplicate value in the given `VCardProperty`
    pub fn pop_duplicates(&mut self, property: VCardProperty) -> Vec<VCF> {
        // each group of duplicate entries will be stored in a separate VCF
        // all the groups will be stored in a Vec<VCF>
        let mut duplicates = Vec::new();
        // avoid panic: if true, the next expression (self.len() - 1) causes a
        // runtime panic
        if self.is_empty() {
            return duplicates;
        }
        let mut i = self.len() - 1; // we start on the last entry

        // we remove the entries who have a duplicate property field, starting
        // from the end of the vector, as it makes index management simpler
        while i > 0 {
            let mut vcf = VCF::new();
            for j in (0..i).rev() {
                if self.entries[i].cmp_property(&self.entries[j], property) {
                    vcf.add_entry(self.entries.remove(j));
                    // keep track of the original object:
                    // index counter has to be decreased as we removed one entry
                    // from the vec
                    i -= 1;
                }
            }
            if !vcf.is_empty() {
                vcf.add_entry(self.entries.remove(i));
                duplicates.push(vcf);
            }
            // this needs to be checked as i is usize and cannot be decremented
            // below 0
            if i > 0 {
                i -= 1;
            }
        }

        duplicates
    }

    /// Find the VCards containing duplicate values in the given property, merge them and return
    /// a VCF object containing the unique entries and the merged entries.
    pub fn merge_from_property(mut self, property: VCardProperty) -> VCF {
        while self.has_duplicate_values_in_property(property) {
            let dups = self.pop_duplicates(property);
            for dup in dups {
                self.add_entry(Vcard::from(dup.entries));
            }
        }
        self.entries.iter_mut().for_each(|vc| vc.keep_single_name());
        self.sort();
        self
    }

    /// Find the VCards containing duplicate values in any supported field (see `enum VCardProperty`), merge them and return
    /// a VCF object containing the merged entries and the unique entries.
    pub fn merge_all(mut self) -> VCF {
        while self.has_duplicates() {
            if let Some(entry) = self.entries.pop() {
                let mut dup = self
                    .entries()
                    .filter(|e| entry.cmp_properties(e))
                    .map(|e| e.to_owned())
                    .collect::<Vec<_>>();

                self.entries.retain(|e| !dup.contains(e));
                if !dup.is_empty() {
                    dup.push(entry);
                    self.add_entry(Vcard::from(dup));
                    // decrement the total entries counter as the instance is being
                    // processed and the added entry is just a merge of existing
                    // entries
                } else {
                    self.entries.insert(0, entry);
                }
            } else {
                // This prevents an infinite loop
                panic!("BUG: shouldn't hit here: an empty VCF has no duplicates");
            }
        }
        self.sort();
        self
    }

    /// Returns a [`VCF`] object contaiing the filtered [`Vcard`]s.
    pub fn filter_by_property_value(&self, property: &VCardProperty, substring: &str) -> VCF {
        self.entries()
            .filter(|e| e.contains_property_value(property, substring))
            .map(|e| e.to_owned())
            .collect::<Vec<Vcard>>()
            .into() // TODO: refactor collect/into
    }
}

#[cfg(test)]
mod tests {
    mod filter;
    mod merge_all;
    mod merge_emails;
    mod merge_names;
    mod merge_phones;
    mod unique_entries;
}
