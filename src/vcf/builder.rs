//! Contains the actual implementation of the methods for VCFBuilder.
//!

use crate::vcf::*;
use crate::{VCFBuilder, VCF};
use std::path::PathBuf;
use std::str::FromStr;

/// Flatten a Result<Result<T,E>,E>, provided that both Err variants have the same type
macro_rules! flatten_result {
    ( $r: expr) => {
        match $r {
            Ok(in_r) => in_r,
            Err(e) => Err(e),
        }
    };
}

impl VCFBuilder {
    /// Creates a new instance of `VCFBuilder`.
    pub fn new() -> Self {
        Self::default()
    }

    /// Sets the builder to split the input data into single VCARD entries which
    /// will be parsed separately.
    pub fn split(self, split: bool) -> Self {
        VCFBuilder { split, ..self }
    }

    /// Sets the builder to silently ignore parsing errors while parsing, ensuring
    /// the Result to be a Ok variant.
    pub fn ignore_errors(self, ignore_errors: bool) -> Self {
        VCFBuilder {
            ignore_errors,
            ..self
        }
    }

    /// Configure the builder to use a single file as input.
    pub fn from_file(self, file_name: PathBuf) -> Self {
        VCFBuilder {
            file_names: Some(vec![file_name]),
            ..self
        }
    }

    /// Configure the builder to use multiple input files.
    pub fn from_files(self, file_names: Vec<PathBuf>) -> Self {
        VCFBuilder {
            file_names: Some(file_names),
            ..self
        }
    }

    /// Configure the builder to use input vcf text from a str.
    pub fn from_data(self, data: &str) -> Self {
        VCFBuilder {
            data: Some(data.to_string()),
            ..self
        }
    }

    /// Tries to build a VCF object, wrapped in a Result, yielding the corresponding errors in case
    /// of failure
    pub fn try_build(self) -> Result<VCF, String> {
        if self.file_names.is_some() && self.data.is_some() {
            return Err("Can't build VCF from both sources".to_string());
        }

        // each arm should yield a Result<VCF, String>
        let vcf = match (self.split, self.ignore_errors) {
            (true, false) => {
                if let Some(files) = self.file_names {
                    // maps each file contained in files vec to be splitted by
                    // split_vcf_file wich returns a Result<Vec<String>, String>
                    //
                    files
                        .iter()
                        .map(|file| {
                            flatten_result!(split_vcf_file(file.to_path_buf()).map(|v| {
                                // v: Vec<String> is then mapped to parse each string with VCF::from_string
                                // VCF::from_string() returns a Result<VCF>, String>
                                //
                                v.iter()
                                    .map(|s| VCF::from_str(s))
                                    // The result of the inner mapping is collected to a Result<Vec<VCF>, String>
                                    // resulting in an outer Result<Result<Vec<VCF>, String>, String>, which is
                                    // flattened by the flatten_result macro
                                    //
                                    // The macro is necessary here because Result::flatten() is still in nightly
                                    //
                                    .collect::<Result<Vec<_>, _>>()
                            }))
                            // The resulting Result<Vec<VCF>, String> is mapped to yield a
                            // Result<VCF, String>
                            //
                            .map(VCF::from)
                        })
                        // In the files.iter() iterator, the objects are now of type
                        // Result<VCF, String>, which are collected into a Result<Vec<VCF>, String>
                        //
                        .collect::<Result<Vec<VCF>, _>>()
                        // The resulting Result<Vec<VCF>, String> is then mapped to Result<VCF, String>
                        //
                        .map(VCF::from)
                } else if let Some(data) = self.data {
                    // split_vcards yields a Vec<String> which is iterated over,
                    // mapped to yield Result<VCF>, String> and collected to a
                    // Result<Vec<VCF>, String>
                    //
                    // The Result is then mapped again to yield Result<VCF, String>
                    split_vcf_str(&data)
                        .iter()
                        .map(|s| VCF::from_str(s))
                        .collect::<Result<Vec<VCF>, _>>()
                        .map(VCF::from)
                } else {
                    Err("No data source for building VCF".to_string())
                }
            }
            (false, false) => {
                if let Some(files) = self.file_names {
                    files
                        .iter()
                        .map(|file| VCF::try_from(file.to_path_buf()))
                        .collect::<Result<Vec<VCF>, _>>()
                        .map(VCF::from)
                } else if let Some(data) = self.data {
                    VCF::from_str(&data)
                } else {
                    Err("No data source for building VCF".to_string())
                }
            }
            (true, true) => {
                if let Some(files) = self.file_names {
                    // maps each file contained in files vec to be splitted by
                    // split_vcf_file which yields a Result<Vec<String>, String>
                    //
                    files
                        .iter()
                        .map(|file| {
                            split_vcf_file(file.to_path_buf())
                                // v: Vec<String> is then mapped to parse each string with VCF::from_string
                                //
                                .map(|v| {
                                    v.iter()
                                        .map(|s| VCF::from_str(s))
                                        // VCF::from_string() returns a Result<VCF>, String>
                                        // which is then filtered to keep Ok(VCF)
                                        // and collected to a Vec<VCF> directly, resulting in a
                                        // Result<Vec<VCF>, String>
                                        //
                                        // The filtering here eliminates the need for flattening
                                        //
                                        .filter_map(|r| r.ok())
                                        .collect::<Vec<VCF>>()
                                })
                                // Result<Vec<VCF>,_> is mapped to Result<VCF,_>
                                .map(VCF::from)
                        })
                        // Vec<Result<VCF, String>, String> is collected to Result<Vec<VCF>, String>
                        .collect::<Result<Vec<VCF>, _>>()
                        // For each file, the resulting  Result<Vec<VCF>, String> is then mapped to
                        // a Result<VCF, String>
                        //
                        .map(VCF::from)
                } else if let Some(data) = self.data {
                    // split_vcards yields a Vec<String> which is iterated over,
                    // mapped to yield Result<VCF>, String>, filtered to keep only
                    // the Ok(VCF) values and collected to a Vec<VCF>
                    //
                    // The Vec<VCF> is fed into VCF::from() and wrapped to yield
                    // a Result<VCF, String>
                    //
                    Ok(VCF::from(
                        split_vcf_str(&data)
                            .iter()
                            .map(|s| VCF::from_str(s))
                            .filter_map(|r| r.ok())
                            .collect::<Vec<VCF>>(),
                    ))
                } else {
                    Err("No data source for building VCF".to_string())
                }
            }
            (false, true) => {
                // returned value is ensured to be Ok(), it contains either the built VCF
                // or an empty one in case of error
                if let Some(files) = self.file_names {
                    files
                        .iter()
                        .map(|file| VCF::try_from(file.to_path_buf()).or_else(|_| Ok(VCF::new())))
                        .collect::<Result<Vec<VCF>, _>>()
                        .map(VCF::from)
                } else if let Some(data) = self.data {
                    VCF::from_str(&data).or_else(|_| Ok(VCF::new()))
                } else {
                    Err("No data source for building VCF".to_string())
                }
            }
        };
        vcf
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn build_empty() {
        let vcf = crate::VCFBuilder::new()
            .from_data("BEGIN:VCARD\r\nEND:VCARD\r\n")
            .try_build();
        assert!(vcf.is_ok());
    }
}
