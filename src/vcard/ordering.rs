//! This module contains the implementation of the [`PartialOrd`] and [`Ord`] traits for [`Vcard`]
//! and the corresponding tests
//!
use crate::vcard::get_sorted_values;
use crate::Vcard;
use std::cmp::Ordering;
use vobject::Property;

impl PartialOrd for Vcard {
    /// This [`PartialOrd`] implementation is based on the [`Ord`] implementation.
    ///
    /// See the [`Ord`] implementation below for details.
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Vcard {
    /// [`Ord`] implementation for [`Vcard`].
    ///
    /// Ordering is done using the "N" field first, then the other fields excepting "EMAIL"
    /// then the "EMAIL" field at the end.
    /// The "N" field is sorted by subvalues, i.e. Surname;Name;;; is splitted on ';' chars.
    /// The "EMAIL" field is also sorted by subvalues, splitted on the '@' char.
    fn cmp(&self, other: &Self) -> Ordering {
        // This closure extracts the specified field from the given sorted vector
        let get_field = {
            |s, v: &mut Vec<Vec<Property>>| {
                v.iter()
                    .position(|e| {
                        if let Some(e) = e.first() {
                            e.name == s
                        } else {
                            false
                        }
                    })
                    .map(|i| v.remove(i))
            }
        };
        let mut sorted_self_values = get_sorted_values(&self.0);
        let mut sorted_other_values = get_sorted_values(&other.0);
        // Extract the EMAIL field from the sorted vectors
        let self_email = get_field("EMAIL", &mut sorted_self_values);
        let other_email = get_field("EMAIL", &mut sorted_other_values);
        // Extract the NAME fields from the sorted vectors
        let self_name = get_field("N", &mut sorted_self_values);
        let other_name = get_field("N", &mut sorted_other_values);

        // Comparison builder closure: allows to compare two Vec<Property> splitting
        // the property values on specified character (useful for ordering NAME
        // and EMAIL fields)
        let cmp = {
            |opt_vec1: Option<Vec<Property>>, opt_vec2: Option<Vec<Property>>, chr| match (
                opt_vec1, opt_vec2,
            ) {
                (Some(vec1), Some(vec2)) => {
                    let cls = || {
                        for (v1, v2) in vec1.iter().zip(vec2.iter()) {
                            for (el_v1, el_v2) in v1
                                .value_as_string()
                                .split(chr)
                                .zip(v2.value_as_string().split(chr))
                            {
                                if el_v1.cmp(el_v2) != Ordering::Equal {
                                    return el_v1.cmp(el_v2);
                                }
                            }
                        }
                        Ordering::Equal
                    };
                    cls()
                }
                (Some(_), None) => Ordering::Greater,
                (None, Some(_)) => Ordering::Less,
                (None, None) => Ordering::Equal,
            }
        };

        // First compare the values in the NAME field, splitted by ';'
        cmp(self_name, other_name, ';')
            // Then compare all the remaining fields, except the EMAIL field, left
            // for the end
            .then(sorted_self_values.cmp(&sorted_other_values))
            .then(cmp(self_email, other_email, '@'))
    }
}

#[cfg(test)]
mod tests {
    use crate::vcard;

    #[test]
    fn partial_ord() {
        let vc1 = vcard!(
            fname: [ "Test" ],
            tel: [ "0223456789" ],
            email: [ "email@example.com" ]
        );
        let vc2 = vcard!(
            fname: [ "Test" ],
            tel: [ "0223456789" ],
            email: [ "email@example.com" ]
        );
        assert_eq!(vc1.partial_cmp(&vc2), Some(std::cmp::Ordering::Equal));

        let vc2 = vcard!(
            fname: [ "Test" ],
            tel: [ "0323456789" ],
            email: [ "email@example.com" ]
        );
        assert_eq!(vc1.partial_cmp(&vc2), Some(std::cmp::Ordering::Less));

        let vc2 = vcard!(
            fname: [ "Test" ],
            tel: [ "0223456789" ],
            email: [ "email2@example.com" ]
        );
        assert_eq!(vc1.partial_cmp(&vc2), Some(std::cmp::Ordering::Less));

        let vc2 = vcard!(
            fname: [ "Test2" ],
            tel: [ "0223456789" ],
            email: [ "email2@example.com" ]
        );
        assert_eq!(vc1.partial_cmp(&vc2), Some(std::cmp::Ordering::Less));
    }

    #[test]
    fn partial_ordering_using_email() {
        let vc1 = vcard!(
            fname: [ "Test" ],
            name: [ "Test;;;;" ],
            tel: [ "0123456789" ],
            email: [ "email@example.com" ]
        );
        let vc2 = vcard!(
            fname: [ "Test" ],
            name: [ "Test;;;;" ],
            tel: [ "0123456789" ],
            email: [ "email2@example.com" ]
        );
        assert_eq!(vc1.partial_cmp(&vc2), Some(std::cmp::Ordering::Less));
    }

    #[test]
    fn partial_ordering_using_email_with_inverted_name() {
        let vc1 = vcard!(
            fname: [ "Test2" ],
            name: [ "Test2;;;;" ],
            email: [ "email@example.com" ]
        );
        let vc2 = vcard!(
            fname: [ "Test" ],
            name: [ "Test;;;;" ],
            email: [ "email2@example.com" ]
        );
        assert_eq!(vc1.partial_cmp(&vc2), Some(std::cmp::Ordering::Greater));
    }

    #[test]
    fn ordering_using_one_field() {
        let vc1 = vcard!(
            fname: [ "Test" ],
            tel: [ "0223456789" ],
            email: [ "email@example.com" ]
        );
        let vc2 = vcard!(
            fname: [ "Test2" ],
            tel: [ "0223456789" ],
            email: [ "email@example.com" ]
        );
        assert!(vc1 < vc2);
    }

    #[test]
    fn ordering_using_two_fields() {
        let vc1 = vcard!(
            fname: [ "Test" ],
            name: [ "Test;;;;" ],
            tel: [ "0223456789" ],
            email: [ "email@example.com" ]
        );
        let vc2 = vcard!(
            fname: [ "Test2" ],
            name: [ "Test2;;;;" ],
            tel: [ "0223456789" ],
            email: [ "email@example.com" ]
        );
        assert!(vc1 < vc2);
    }

    #[test]
    fn ordering_using_second_field() {
        let vc1 = vcard!(
            fname: [ "Test" ],
            name: [ "Test;;;;" ],
            tel: [ "0223456789" ],
            email: [ "email@example.com" ]
        );
        let vc2 = vcard!(
            fname: [ "Test" ],
            name: [ "T;;;;" ],
            tel: [ "0223456789" ],
            email: [ "email@example.com" ]
        );
        assert!(vc1 > vc2);
    }

    #[test]
    fn ordering_using_tel() {
        let vc1 = vcard!(
            fname: [ "Test" ],
            name: [ "Test;;;;" ],
            tel: [ "0123456789" ],
            email: [ "email@example.com" ]
        );
        let vc2 = vcard!(
            fname: [ "Test" ],
            name: [ "Test;;;;" ],
            tel: [ "0223456789" ],
            email: [ "email@example.com" ]
        );
        assert!(vc1 < vc2);
    }

    #[test]
    fn ordering_using_email() {
        let vc1 = vcard!(
            fname: [ "Test" ],
            name: [ "Test;;;;" ],
            tel: [ "0123456789" ],
            email: [ "email@example.com" ]
        );
        let vc2 = vcard!(
            fname: [ "Test" ],
            name: [ "Test;;;;" ],
            tel: [ "0123456789" ],
            email: [ "email2@example.com" ]
        );
        assert_eq!(vc1.cmp(&vc2), std::cmp::Ordering::Less);
    }

    #[test]
    fn ordering_using_email_with_inverted_name() {
        let vc1 = vcard!(
            fname: [ "Test2" ],
            email: [ "email@example.com" ]
        );
        let vc2 = vcard!(
            fname: [ "Test" ],
            email: [ "email2@example.com" ]
        );
        assert_eq!(vc1.cmp(&vc2), std::cmp::Ordering::Greater);
    }

    #[test]
    fn complex_ordering1() {
        let v1 = vcard!(
                name: [ "Test3;;;;" ],
                fname: [ "Test3" ],
                tel: [ "0213456789" ],
                email: [ "test@example.com" ]
        );
        let v2 = vcard!(
                name: [ "Test4;;;;" ],
                fname: [ "Test" ],
                tel: [ "0312456789" ],
                email: [ "test4@example.com" ]
        );
        assert!(v1 < v2);
    }

    #[test]
    fn complex_ordering2() {
        let v1 = vcard!(
                name: [ "Test2;;;;" ],
                fname: [ "Test2" ],
                tel: [ "0123456789" ],
                email: [ "test2@example.com" ]
        );
        let v2 = vcard!(
                name: [ "Test;;;;" ],
                fname: [ "Test" ],
                tel: [ "0123456789" ],
                email: [ "test@example.com" ]
        );
        assert!(v1 > v2);
    }

    #[test]
    fn complex_ordering3() {
        let v1 = vcard!(
                name: [ "Test2;;;;" ],
                fname: [ "Test2" ],
                tel: [ "0123456789" ],
                email: [ "test2@example.com" ]
        );
        let v2 = vcard!(
                name: [ "Test3;;;;" ],
                fname: [ "Test3" ],
                tel: [ "0213456789" ],
                email: [ "test@example.com" ]
        );
        assert!(v1 < v2);
    }
}
