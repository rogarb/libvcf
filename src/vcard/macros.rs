/// Generates a Vcard object given a set of parameters
///
/// Currently it can generate a Vcard from any combination of name, fname, email
/// and tel with any number of field.
///
/// The syntax is vcard!(field: [value, ...], ... field: [value, ...])
#[macro_export]
macro_rules! vcard {
    //****************************\\
    //** builder internal rules **\\
    //****************************\\
    (@builder) => {
        vobject::vcard::VcardBuilder::new()
    };
    (@builder $name:ident: $value:literal) => {
        vcard!(@builder $name: [ $value ])
    };
    (@builder $name:ident: [ $( $value:literal ),+ ]) => {
        match stringify!($name) {
            "fname" => {
                vcard!(@builder)
                    $(
                        .with_fullname($value.to_string())
                    )+
            },
            "name" => {
                vcard!(@builder)
                    $(
                        .with_name(
                            std::collections::BTreeMap::new(),
                            vobject::vcard::Name::from_raw($value.to_string()).surname(),
                            vobject::vcard::Name::from_raw($value.to_string()).given_name(),
                            vobject::vcard::Name::from_raw($value.to_string()).additional_names(),
                            vobject::vcard::Name::from_raw($value.to_string()).honorific_prefixes(),
                            vobject::vcard::Name::from_raw($value.to_string()).honorific_suffixes()
                        )
                    )+
            },
            "tel" => {
                vcard!(@builder)
                    $(
                        .with_tel(std::collections::BTreeMap::new(), $value.to_string())
                    )+
            },
            "email" => {
                vcard!(@builder)
                    $(
                        .with_email($value.to_string())
                    )+
            },
            _ => {
                vcard!(@builder)
            }
        }
    };
    (@builder $name:ident: [ $( $value:literal ),+ ], $( $name2:ident : [ $( $value2:literal ),+ ] ),+  ) => {
        match stringify!($name) {
            "fname" => {
                vcard!(@builder $( $name2 : [ $( $value2 ),+ ] ),+)
                    $(
                        .with_fullname($value.to_string())
                    )+
            },
            "name" => {
                vcard!(@builder $( $name2 : [ $( $value2 ),+ ] ),+)
                    $(
                        .with_name(
                            std::collections::BTreeMap::new(),
                            vobject::vcard::Name::from_raw($value.to_string()).surname(),
                            vobject::vcard::Name::from_raw($value.to_string()).given_name(),
                            vobject::vcard::Name::from_raw($value.to_string()).additional_names(),
                            vobject::vcard::Name::from_raw($value.to_string()).honorific_prefixes(),
                            vobject::vcard::Name::from_raw($value.to_string()).honorific_suffixes()
                        )
                    )+
            },
            "tel" => {
                vcard!(@builder $( $name2 : [ $( $value2 ),+ ] ),+)
                    $(
                        .with_tel(std::collections::BTreeMap::new(), $value.to_string())
                    )+
            },
            "email" => {
                vcard!(@builder $( $name2 : [ $( $value2 ),+ ] ),+)
                    $(
                        .with_email($value.to_string())
                    )+
            },
            _ => {
                vcard!(@builder $( $name2 : [ $( $value2 ),+ ] ),+)
            }
        }
    };
    //*****************\\
    //** public rule **\\
    //*****************\\
    ( $( $name:ident : [ $( $values:literal ),+ ] ),+ ) => {
        $crate::Vcard::from(vcard!(@builder $( $name : [ $( $values ),+ ] ),+ ).build().expect("Error: invalid input for macro vcard!()"))
    };
}

#[cfg(test)]
mod tests {
    #[test]
    fn macro_vcard() {
        // this test doesn't test anything but will make test compilation fail if the vcard! macro
        // doesn't handle its input properly
        let _vc = vcard!(fname: [ "Test" ],
                         name: [ "Test;;;;" ],
                         tel: [ "0223456789", "0123456789", "0123456789", "0223456789" ],
                         email: [ "email@example.com", "email2@example.com", "email@example.com" ]
        );
        let _vc = vcard!(fname: [ "Test" ],
                         tel: [ "0223456789", "0123456789", "0123456789", "0223456789" ],
                         email: [ "email@example.com", "email2@example.com", "email@example.com" ]
        );
        let _vc = vcard!(name: [ "Test;;;;" ],
                         tel: [ "0223456789", "0123456789", "0123456789", "0223456789" ],
                         email: [ "email@example.com", "email2@example.com", "email@example.com" ]
        );
        let _vc = vcard!(fname: [ "Test" ],
                         name: [ "Test;;;;" ],
                         email: [ "email@example.com", "email2@example.com", "email@example.com" ]
        );
        let _vc = vcard!(fname: [ "Test" ],
                         email: [ "email@example.com", "email2@example.com", "email@example.com" ]
        );
        let _vc = vcard!(name: [ "Test;;;;" ],
                         email: [ "email@example.com", "email2@example.com", "email@example.com" ]
        );
        let _vc = vcard!(fname: [ "Test" ],
                         name: [ "Test;;;;" ],
                         tel: [ "0223456789", "0123456789", "0123456789", "0223456789" ]
        );
        let _vc = vcard!(fname: [ "Test" ],
                         tel: [ "0223456789", "0123456789", "0123456789", "0223456789" ]
        );
        let _vc = vcard!(name: [ "Test;;;;" ],
                         tel: [ "0223456789", "0123456789", "0123456789", "0223456789" ]
        );
        let _vc = vcard!(fname: [ "Test" ]);
        let _vc = vcard!(name: [ "Test;;;;" ]);
        let _vc = vcard!(tel: [ "0223456789", "0123456789", "0123456789", "0223456789" ] );
        let _vc =
            vcard!(email: [ "email@example.com", "email2@example.com", "email@example.com" ] );
        let _vc = vcard!(tel: [ "0223456789", "0123456789", "0123456789", "0223456789" ],
                         email: [ "email@example.com", "email2@example.com", "email@example.com" ]
        );
        let _vc = vcard!(email: [ "email@example.com", "email2@example.com", "email@example.com" ],
                         tel: [ "0223456789", "0123456789", "0123456789", "0223456789" ]
        );
    }
}
