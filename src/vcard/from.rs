//! This module contains the From<T> and TryFrom<T> implementations for `Vcard`
//!
#[cfg(feature = "ical")]
use crate::dbg;
use crate::Vcard;
#[cfg(feature = "ical")]
use ical::parser::vcard::component::VcardContact;
use std::ops::Deref;

impl From<vobject::Vcard> for Vcard {
    /// Converts a vobject::Vcard object to a Vcard object
    // TODO: this should be moved to TryFrom and we should check that
    // VobjectVcard's Component.name == "VCARD"
    fn from(vc: vobject::Vcard) -> Self {
        // Deref for vobject::Vcard yields a reference to the wrapped Component
        Self(vc.deref().props.clone())
    }
}

#[cfg(feature = "ical")]
impl From<VcardContact> for Vcard {
    /// Converts an ical::parser::vcard::component::VcardContact object to a Vcard object.
    fn from(vcard: VcardContact) -> Self {
        let mut new = Self::new();
        for ical_prop in vcard.properties {
            if let Some(value) = ical_prop.value {
                dbg!(ical_prop.name);
                dbg!(value);
                let mut vobj_prop = vobject::Property::new(ical_prop.name.clone(), value);
                if let Some(params) = ical_prop.params {
                    for (key, vec) in params {
                        for value in vec {
                            vobj_prop.params.insert(key.to_string(), value.to_string());
                        }
                    }
                }
                new.0
                    .entry(ical_prop.name.to_string())
                    .and_modify(|vec| vec.push(vobj_prop.clone()))
                    .or_insert_with(|| vec![vobj_prop]);
            }
        }
        new
    }
}

impl From<Vec<Vcard>> for Vcard {
    /// Merges the Vcard objects in the Vec into a new Vcard.
    ///
    /// All entries are moved into a single Vcard object which is then reduced to
    /// remove duplicate properties.
    fn from(vec: Vec<Vcard>) -> Vcard {
        vec.into_iter()
            .reduce(|mut acc, vcard| {
                vcard.0.iter().for_each(|(key, prop)| {
                    acc.0
                        .entry(key.to_string())
                        .and_modify(|entry| entry.append(&mut prop.clone()))
                        .or_insert_with(|| prop.to_vec());
                });
                acc
            })
            .map_or(Vcard::default(), |vcard| vcard.reduce())
    }
}

#[cfg(test)]
mod tests {
    use crate::vcard;

    #[cfg(feature = "ical")]
    macro_rules! vcardcontact {
        (@prop $name: literal, $value: literal) => {
            ical::property::Property {
                name: $name.into(),
                params: None,
                value: Some($value.into())
            }
        };

        (@prop $([$name: literal, $value: literal]),+) => {
            $( vcardcontact!(@prop $name, $value) ),+
        };
        ([$([$name: literal, $value: literal]),+]) => {
            ical::parser::vcard::component::VcardContact { properties: vec![$( vcardcontact!(@prop [$name, $value]) ),+] }
        };
    }

    #[test]
    #[cfg(feature = "ical")]
    fn from_vcardcontact() {
        let input = vcardcontact!([
            ["FN", "FTest"],
            ["N", "Test;;;;"],
            ["EMAIL", "test@example.com"],
            ["TEL", "0123456789"]
        ]);

        dbg!(input.clone());
        let result = super::Vcard::from(input);
        let expected = vcard!(
            fname: [ "FTest" ],
            name: [ "Test;;;;" ],
            tel: ["0123456789"],
            email: ["test@example.com"]
        );

        assert_eq!(result, expected);
    }

    #[test]
    fn from_vec() {
        let vec: Vec<super::Vcard> = vec![
            vcard!(fname: [ "Test" ], email: [ "email@example.com" ], tel: [ "0123456789" ]),
            vcard!(fname: [ "Test" ], email: [ "email2@example.com" ], tel: [ "0123456789" ]),
            vcard!(fname: [ "Test" ], email: [ "email@example.com" ], tel: [ "0223456789" ]),
        ];
        let result = super::Vcard::from(vec);
        let expect = super::Vcard::from(vcard!(
            fname: [ "Test" ],
            tel: [ "0223456789", "0123456789" ],
            email: [ "email@example.com", "email2@example.com" ]
        ));

        assert_eq!(result, expect);
    }
}
