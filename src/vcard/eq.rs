//! This module contains the implementation of the [`PartialEq`] and [`Eq`] traits for [`Vcard`]
//! and the corresponding tests
//!
use super::{property_eq, property_sorting};
use crate::Vcard;

impl PartialEq for Vcard {
    /// Tests equality of two Vcard objects. In this implementation, field ordering doesn't matter
    /// as we compare reordered Vec<Property>
    fn eq(&self, other: &Self) -> bool {
        // the sort and compare method has to be used instead of a HashSet/HashMap method
        // as vobject::property::Property doesn't implement the Hash trait
        self.0
            .iter()
            .zip(other.0.iter())
            .all(|((prop_name_s, props_s), (prop_name_o, props_o))| {
                let mut props_s = props_s.clone();
                let mut props_o = props_o.clone();
                props_s.sort_by(property_sorting);
                props_o.sort_by(property_sorting);
                prop_name_s == prop_name_o
                    && props_s
                        .iter()
                        .zip(props_o.iter())
                        .all(|(x, y)| property_eq(x, y))
            })
    }
}

impl Eq for Vcard {}

#[cfg(test)]
mod tests {
    use crate::vcard;

    #[test]
    fn equal_vcards() {
        let vc1 = vcard!(
            fname: [ "Test" ],
            tel: [ "0223456789" ],
            email: [ "email@example.com" ]
        );
        let vc2 = vcard!(
            fname: [ "Test" ],
            tel: [ "0223456789" ],
            email: [ "email@example.com" ]
        );
        assert_eq!(vc1, vc2);
    }

    #[test]
    fn non_equal_cards() {
        let vc1 = vcard!(
            fname: [ "Test" ],
            tel: [ "0223456789" ],
            email: [ "email@example.com" ]
        );
        let vc2 = vcard!(
            fname: [ "Test" ],
            tel: [ "0323456789" ],
            email: [ "email2@example.com" ]
        );
        assert_ne!(vc1, vc2);
    }
}
