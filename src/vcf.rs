//! Contains utility functions for vcf data manipulation.
//!
//! It also contains all the implementation details of [`VCF`] and
//! [`VCFBuilder`](crate::VCFBuilder).
//!
use crate::ui;
use crate::vcardproperty::VCardProperty;
use crate::Vcard;
use crate::VCF;
use std::fs;
use std::fs::File;
use std::io::prelude::*;
use std::path::PathBuf;

mod builder;
mod display;
mod eq;
mod from;
mod processing;
mod stats;

// constructor
impl VCF {
    /// Creates a [`VCF`] instance based on defaults.
    pub fn new() -> VCF {
        VCF::default()
    }
}

/// Various utility methods for [`VCF`].
impl VCF {
    /// Returns the number of entries in the [`VCF`] instance.
    pub fn len(&self) -> usize {
        self.entries.len()
    }

    /// Returns [`true`] if the [`VCF`] contains no entry, [`false`] otherwise.
    pub fn is_empty(&self) -> bool {
        self.entries.is_empty()
    }

    /// Returns an [`Iterator`] over the entries in the [`VCF`] instance.
    pub fn entries(&self) -> std::slice::Iter<'_, Vcard> {
        self.entries.iter()
    }

    /// Writes the data contained in the [`VCF`] instance into a file in the VCARD format.
    pub fn write_to_file(&self, filename: PathBuf) -> Result<(), String> {
        // set the extension if not present
        let mut filename = filename;
        if filename.extension().is_none() {
            filename.set_extension("vcf");
        } else if let Some(ext) = filename.extension() {
            if ext.to_ascii_lowercase().to_str().unwrap() != "vcf"
                && !ui::ask_user_confirmation(
                    "Warning: writing a vcard file with a non standard extension. Continue?",
                )
            {
                return Err("Aborted by user".to_string());
            }
        }

        if filename.exists()
            && !ui::ask_user_confirmation("Destination file already exists. Overwrite?")
        {
            return Err("Aborted by user".to_string());
        }

        let filename = filename
            .to_str()
            .ok_or(format!("Unable to convert {} to str", filename.display()))?;
        fs::write(filename, self.to_string())
            .map_err(|e| format!("Unable to write to file {}: {}", filename, e))?;
        Ok(())
    }

    /// Merges the content of a [`VCF`] object into self.
    pub fn merge(&mut self, other: VCF) {
        other.entries.into_iter().for_each(|e| self.add_entry(e));
    }

    /// Reverses the entries in self, consuming self and returning the reversed object.
    pub fn rev(self) -> VCF {
        VCF {
            entries: self.entries.into_iter().rev().collect(),
        }
    }

    /// Adds a new [`Vcard`] entry to self.
    pub fn add_entry(&mut self, entry: Vcard) {
        self.entries.push(entry);
    }

    /// Returns true if a field of a [`Vcard`] entry has the same value as a field
    /// of another [`Vcard`] entry.
    pub fn has_duplicates(&self) -> bool {
        self.entries().enumerate().any(|(i1, e)| {
            self.entries()
                .enumerate()
                .filter(|(i2, _)| i1 != *i2)
                .any(|(_, f)| e.cmp_properties(f))
        })
    }

    /// Returns true if the given property of a [`Vcard`] entry has a duplicate value
    /// in another [`Vcard`] entry.
    pub fn has_duplicate_values_in_property(&self, property: VCardProperty) -> bool {
        self.entries().enumerate().any(|(i1, e)| {
            self.entries()
                .enumerate()
                .filter(|(i2, _)| i1 != *i2)
                .any(|(_, f)| e.cmp_property(f, property))
        })
    }

    /// Sorts the instance in place
    pub fn sort(&mut self) {
        self.entries.sort();
    }
}

// utility functions
/// Splits the input vcf file into a [`Vec<String>`], each [`String`] containing a single VCARD entry.
pub fn split_vcf_file(file: PathBuf) -> Result<Vec<String>, String> {
    let mut data = String::new();
    //
    match File::open(&file) {
        Ok(mut file_handle) => {
            if let Err(e) = file_handle.read_to_string(&mut data) {
                return Err(format!("unable to read file {}: {e}", file.display()));
            }
        }
        Err(e) => {
            return Err(format!("unable to open file {}: {e}", file.display()));
        }
    }
    Ok(split_vcf_str(&data))
}

/// Splits the input str containing vcf data into a [`Vec<String>`], each [`String`]
/// containing a single VCARD entry.
pub fn split_vcf_str(data: &str) -> Vec<String> {
    let mut s = String::new();
    let mut v = Vec::new();
    for line in data.lines() {
        if line.contains("BEGIN:VCARD") {
            s.clear();
        }
        s.push_str(line);
        s.push_str("\r\n");
        if line.contains("END:VCARD") {
            v.push(s.clone());
        }
    }
    v
}

#[cfg(test)]
mod tests {
    use super::VCF;
    use std::str::FromStr;

    #[test]
    fn merge() {
        let mut result = vcf!(
        [
            "VERSION": [ "3.0" ],
            "N": [ "Test;;;;" ],
            "FN": [ "Test" ],
            "TEL": [ "0123456789" ],
            "EMAIL": [ "test@example.com" ]
        ]
        );
        let merge = vec![
            vcf!(
                [
                    "VERSION": [ "3.0" ],
                    "N": [ "Test2;;;;" ],
                    "FN": [ "Test2" ],
                    "TEL": [ "0123456789" ],
                    "EMAIL": [ "test2@example.com" ]
                ],
                [
                    "VERSION": [ "3.0" ],
                    "N": [ "Test3;;;;" ],
                    "FN": [ "Test3" ],
                    "TEL": [ "0213456789" ],
                    "EMAIL": [ "test@example.com" ]
                ]
            ),
            vcf!(
                [
                    "VERSION": [ "3.0" ],
                    "N": [ "Test4;;;;" ],
                    "FN": [ "Test" ],
                    "TEL": [ "0312456789" ],
                    "EMAIL": [ "test4@example.com" ]
                ]
            ),
        ];
        for v in merge.into_iter() {
            result.merge(v);
        }
        let expected = vcf!(
            [
                "VERSION": [ "3.0" ],
                "N": [ "Test;;;;" ],
                "FN": [ "Test" ],
                "TEL": [ "0123456789" ],
                "EMAIL": [ "test@example.com" ]
            ],
            [
                "VERSION": [ "3.0" ],
                "N": [ "Test2;;;;" ],
                "FN": [ "Test2" ],
                "TEL": [ "0123456789" ],
                "EMAIL": [ "test2@example.com" ]
            ],
            [
                "VERSION": [ "3.0" ],
                "N": [ "Test3;;;;" ],
                "FN": [ "Test3" ],
                "TEL": [ "0213456789" ],
                "EMAIL": [ "test@example.com" ]
            ],
            [
                "VERSION": [ "3.0" ],
                "N": [ "Test4;;;;" ],
                "FN": [ "Test" ],
                "TEL": [ "0312456789" ],
                "EMAIL": [ "test4@example.com" ]
            ]
        );

        assert_eq!(result, expected);
    }

    #[test]
    fn rev() {
        let result = VCF::from_str(
            "\
BEGIN:VCARD\r\n\
VERSION:4.0\r\n\
FN:FTest\r\n\
N:Test;;;;\r\n\
TEL:0123456789\r\n\
EMAIL:test@example.com\r\n\
REV:20220605T134526Z\r\n\
END:VCARD\r\n\
BEGIN:VCARD\r\n\
VERSION:4.0\r\n\
FN:FTest2\r\n\
N:Test2;;;;\r\n\
TEL:0223456789\r\n\
EMAIL:test2@example.com\r\n\
REV:20220605T134526Z\r\n\
END:VCARD\r\n\
BEGIN:VCARD\r\n\
VERSION:4.0\r\n\
FN:FTest3\r\n\
N:Test3:q3;;;;\r\n\
TEL:0323456789\r\n\
EMAIL:test3:q3@example.com\r\n\
REV:20220605T134526Z\r\n\
END:VCARD\r\n\
",
        )
        .expect("BUG: the imput should be valid");

        let expected = VCF::from_str(
            "\
BEGIN:VCARD\r\n\
VERSION:4.0\r\n\
FN:FTest3\r\n\
N:Test3:q3;;;;\r\n\
TEL:0323456789\r\n\
EMAIL:test3:q3@example.com\r\n\
REV:20220605T134526Z\r\n\
END:VCARD\r\n\
BEGIN:VCARD\r\n\
VERSION:4.0\r\n\
FN:FTest2\r\n\
N:Test2;;;;\r\n\
TEL:0223456789\r\n\
EMAIL:test2@example.com\r\n\
REV:20220605T134526Z\r\n\
END:VCARD\r\n\
BEGIN:VCARD\r\n\
VERSION:4.0\r\n\
FN:FTest\r\n\
N:Test;;;;\r\n\
TEL:0123456789\r\n\
EMAIL:test@example.com\r\n\
REV:20220605T134526Z\r\n\
END:VCARD\r\n\
",
        )
        .expect("BUG: the imput should be valid");

        assert_eq!(result.rev(), expected);
    }

    #[test]
    fn has_duplicates() {
        let vcf = vcf!(
            [
                "VERSION": [ "3.0" ],
                "N": [ "Test;;;;" ],
                "FN": [ "Test" ],
                "TEL": [ "0123456789" ],
                "EMAIL": [ "test@example.com" ]
            ],
            [
                "VERSION": [ "3.0" ],
                "N": [ "Test2;;;;" ],
                "FN": [ "Test2" ],
                "TEL": [ "0213456789" ],
                "EMAIL": [ "test2@example.com" ]
            ]
        );
        assert!(!vcf.has_duplicates());

        let vcf = vcf!(
            [
                "VERSION": [ "3.0" ],
                "N": [ "Test;;;;" ],
                "FN": [ "Test" ],
                "TEL": [ "0123456789" ],
                "EMAIL": [ "test@example.com" ]
            ],
            [
                "VERSION": [ "3.0" ],
                "N": [ "Test2;;;;" ],
                "FN": [ "Test2" ],
                "TEL": [ "0123456789" ],
                "EMAIL": [ "test2@example.com" ]
            ],
            [
                "VERSION": [ "3.0" ],
                "N": [ "Test3;;;;" ],
                "FN": [ "Test3" ],
                "TEL": [ "0213456789" ],
                "EMAIL": [ "test@example.com" ]
            ],
            [
                "VERSION": [ "3.0" ],
                "N": [ "Test4;;;;" ],
                "FN": [ "Test" ],
                "TEL": [ "0312456789" ],
                "EMAIL": [ "test4@example.com" ]
            ]
        );
        assert!(vcf.has_duplicates());

        let vcf = vcf!(
            [
                "VERSION": [ "3.0" ],
                "N": [ "Test;;;;" ],
                "FN": [ "Test" ],
                "TEL": [ "0123456789" ],
                "EMAIL": [ "test@example.com" ]
            ],
            [
                "VERSION": [ "3.0" ],
                "N": [ "Test;;;;" ],
                "FN": [ "Test" ],
                "TEL": [ "0123456789" ],
                "EMAIL": [ "test@example.com" ]
            ]
        );
        assert!(vcf.has_duplicates());

        let vcf = vcf!(
            [
                "VERSION": [ "3.0" ],
                "N": [ "Test;;;;" ],
                "FN": [ "Test" ],
                "TEL": [ "0123456789" ],
                "EMAIL": [ "test@example.com" ]
            ]
        );
        assert!(!vcf.has_duplicates());
    }

    #[test]
    fn has_duplicate_values_in_property() {
        use crate::vcardproperty::VCardProperty;
        let vcf = vcf!(
            [
                "VERSION": [ "3.0" ],
                "N": [ "Test;;;;" ],
                "FN": [ "Test" ],
                "TEL": [ "0123456789" ],
                "EMAIL": [ "test@example.com" ]
            ],
            [
                "VERSION": [ "3.0" ],
                "N": [ "Test2;;;;" ],
                "FN": [ "Test2" ],
                "TEL": [ "0213456789" ],
                "EMAIL": [ "test2@example.com" ]
            ]
        );
        assert!(!vcf.has_duplicate_values_in_property(VCardProperty::Email));

        let vcf = vcf!(
            [
                "VERSION": [ "3.0" ],
                "N": [ "Test;;;;" ],
                "FN": [ "Test" ],
                "TEL": [ "0123456789" ],
                "EMAIL": [ "test@example.com" ]
            ],
            [
                "VERSION": [ "3.0" ],
                "N": [ "Test2;;;;" ],
                "FN": [ "Test2" ],
                "TEL": [ "0123456789" ],
                "EMAIL": [ "test2@example.com" ]
            ],
            [
                "VERSION": [ "3.0" ],
                "N": [ "Test3;;;;" ],
                "FN": [ "Test3" ],
                "TEL": [ "0213456789" ],
                "EMAIL": [ "test@example.com" ]
            ],
            [
                "VERSION": [ "3.0" ],
                "N": [ "Test4;;;;" ],
                "FN": [ "Test" ],
                "TEL": [ "0312456789" ],
                "EMAIL": [ "test4@example.com" ]
            ]
        );
        assert!(vcf.has_duplicate_values_in_property(VCardProperty::Phone));
        assert!(vcf.has_duplicate_values_in_property(VCardProperty::Email));
        assert!(!vcf.has_duplicate_values_in_property(VCardProperty::Name));

        let vcf = vcf!(
            [
                "VERSION": [ "3.0" ],
                "N": [ "Test;;;;" ],
                "FN": [ "Test" ],
                "TEL": [ "0123456789" ],
                "EMAIL": [ "test@example.com" ]
            ],
            [
                "VERSION": [ "3.0" ],
                "N": [ "Test;;;;" ],
                "FN": [ "Test" ],
                "TEL": [ "0123456789" ],
                "EMAIL": [ "test@example.com" ]
            ]
        );
        assert!(vcf.has_duplicate_values_in_property(VCardProperty::Name));

        let vcf = vcf!(
            [
                "VERSION": [ "3.0" ],
                "N": [ "Test;;;;" ],
                "FN": [ "Test" ],
                "TEL": [ "0123456789" ],
                "EMAIL": [ "test@example.com" ]
            ]
        );
        assert!(!vcf.has_duplicate_values_in_property(VCardProperty::Name));
        assert!(!vcf.has_duplicate_values_in_property(VCardProperty::Email));
        assert!(!vcf.has_duplicate_values_in_property(VCardProperty::Phone));
    }

    #[test]
    fn sort_1entry() {
        let mut result = vcf!(
            [
                "VERSION": [ "3.0" ],
                "N": [ "Test;;;;" ],
                "FN": [ "Test" ],
                "TEL": [ "0123456789" ],
                "EMAIL": [ "test@example.com" ]
            ]
        );
        result.sort();
        let expected = vcf!(
            [
                "VERSION": [ "3.0" ],
                "N": [ "Test;;;;" ],
                "FN": [ "Test" ],
                "TEL": [ "0123456789" ],
                "EMAIL": [ "test@example.com" ]
            ]
        );
        assert_eq!(result, expected);
    }

    #[test]
    fn sort_2entries_already_sorted() {
        let mut result = vcf!(
            [
                "VERSION": [ "3.0" ],
                "N": [ "Test;;;;" ],
                "FN": [ "Test" ],
                "TEL": [ "0123456789" ],
                "EMAIL": [ "test@example.com" ]
            ],
            [
                "VERSION": [ "3.0" ],
                "N": [ "Test2;;;;" ],
                "FN": [ "Test2" ],
                "TEL": [ "0123456789" ],
                "EMAIL": [ "test@example.com" ]
            ]
        );
        result.sort();
        let expected = vcf!(
            [
                "VERSION": [ "3.0" ],
                "N": [ "Test;;;;" ],
                "FN": [ "Test" ],
                "TEL": [ "0123456789" ],
                "EMAIL": [ "test@example.com" ]
            ],
            [
                "VERSION": [ "3.0" ],
                "N": [ "Test2;;;;" ],
                "FN": [ "Test2" ],
                "TEL": [ "0123456789" ],
                "EMAIL": [ "test@example.com" ]
            ]
        );
        assert_eq!(result, expected);
    }

    #[test]
    fn sort_4entries_unsorted() {
        let mut result = vcf!(
            [
                "VERSION": [ "3.0" ],
                "N": [ "Test3;;;;" ],
                "FN": [ "Test3" ],
                "TEL": [ "0213456789" ],
                "EMAIL": [ "test@example.com" ]
            ],
            [
                "VERSION": [ "3.0" ],
                "N": [ "Test4;;;;" ],
                "FN": [ "Test" ],
                "TEL": [ "0312456789" ],
                "EMAIL": [ "test4@example.com" ]
            ],
            [
                "VERSION": [ "3.0" ],
                "N": [ "Test2;;;;" ],
                "FN": [ "Test2" ],
                "TEL": [ "0123456789" ],
                "EMAIL": [ "test2@example.com" ]
            ],
            [
                "VERSION": [ "3.0" ],
                "N": [ "Test;;;;" ],
                "FN": [ "Test" ],
                "TEL": [ "0123456789" ],
                "EMAIL": [ "test@example.com" ]
            ]
        );
        result.sort();
        let expected = vcf!(
            [
                "VERSION": [ "3.0" ],
                "N": [ "Test;;;;" ],
                "FN": [ "Test" ],
                "TEL": [ "0123456789" ],
                "EMAIL": [ "test@example.com" ]
            ],
            [
                "VERSION": [ "3.0" ],
                "N": [ "Test2;;;;" ],
                "FN": [ "Test2" ],
                "TEL": [ "0123456789" ],
                "EMAIL": [ "test2@example.com" ]
            ],
            [
                "VERSION": [ "3.0" ],
                "N": [ "Test3;;;;" ],
                "FN": [ "Test3" ],
                "TEL": [ "0213456789" ],
                "EMAIL": [ "test@example.com" ]
            ],
            [
                "VERSION": [ "3.0" ],
                "N": [ "Test4;;;;" ],
                "FN": [ "Test" ],
                "TEL": [ "0312456789" ],
                "EMAIL": [ "test4@example.com" ]
            ]
        );
        assert_eq!(result, expected);
    }

    #[test]
    fn sort_2_equal_entries() {
        let mut result = vcf!(
            [
                "VERSION": [ "3.0" ],
                "N": [ "Test;;;;" ],
                "FN": [ "Test" ],
                "TEL": [ "0123456789" ],
                "EMAIL": [ "test@example.com" ]
            ],
            [
                "VERSION": [ "3.0" ],
                "N": [ "Test;;;;" ],
                "FN": [ "Test" ],
                "TEL": [ "0123456789" ],
                "EMAIL": [ "test@example.com" ]
            ]
        );
        result.sort();
        let expected = vcf!(
            [
                "VERSION": [ "3.0" ],
                "N": [ "Test;;;;" ],
                "FN": [ "Test" ],
                "TEL": [ "0123456789" ],
                "EMAIL": [ "test@example.com" ]
            ],
            [
                "VERSION": [ "3.0" ],
                "N": [ "Test;;;;" ],
                "FN": [ "Test" ],
                "TEL": [ "0123456789" ],
                "EMAIL": [ "test@example.com" ]
            ]
        );
        assert_eq!(result, expected);
    }
}
