// validator module: implements the different validators
use super::{Parser, Parsers};
use crate::vcf::split_vcf_str;
#[cfg(feature = "ical")]
use ical::VcardParser;
#[cfg(feature = "ical")]
use std::io::BufReader;
use vobject::vcard::Vcard;

impl Parser {
    /// is_valid: returns true if the data is deemed to be valid by the selected
    /// validation function
    pub fn is_valid(&self) -> bool {
        match self.parser_crate {
            Parsers::Vobject => split_vcf_str(&self.data)
                .iter()
                .all(|data| Vcard::build(data).is_ok()),
            #[cfg(feature = "ical")]
            Parsers::Ical => {
                VcardParser::new(BufReader::new(&mut self.data.as_bytes())).all(|e| e.is_ok())
            }
        }
    }

    /// validate: returns Ok(()) if the data is deemed to be valid by the selected
    /// validation function, else returns a String containing information regarding
    /// the non validity of the data
    pub fn validate(&self) -> Result<(), String> {
        let mut err = String::new();
        match self.parser_crate {
            Parsers::Vobject => {
                for vc in split_vcf_str(&self.data) {
                    if let Err(e) = Vcard::build(&vc) {
                        err.push_str(format!("{}\n", e).as_str());
                    }
                }
            }
            #[cfg(feature = "ical")]
            Parsers::Ical => {
                for vcard in VcardParser::new(BufReader::new(&mut self.data.as_bytes())) {
                    if let Err(e) = vcard {
                        err.push_str(format!("{}\n", e).as_str());
                    }
                }
            }
        }
        if !err.is_empty() {
            Err(err)
        } else {
            Ok(())
        }
    }
}
