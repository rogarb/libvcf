//! This lib offers different tools to process and generate VCard Format files (.vcf files).
//!
//! The main interface is the [`VCF`] struct and its associated builder, [`VCFBuilder`].
//!
//! Each VCARD entry is stored in a [`Vcard`] object (for more information, see the
//! relevant part of the documentation).
//!
//! The library has built-in support for handling abook addressbook files through
//! the [`abook`](<https://abook.sourceforge.io>) program. Parsing abook files
//! without `abook` available in the `PATH` will result in an error.
//!
//! The [`Stats`](stats::Stats) structure can be used to compute and store metrics related to
//! a [`VCF`] object. See the [`stats`] module.
//!
//! # Feature flags
//!
//! All feature flags are disabled by default.
//!
//! - `default-parser-ical`: use the crate [`ical`](<https://crates.io/crates/ical>)
//!   for providing the VCF file parsing functionality (the library uses the
//!   crate [`vobject`](<https://crates.io/crates/vobject>) by default).
//! - `ical`: add support for the crate [`ical`](<https://crates.io/crates/ical>)
//!   as a possible VCF [parser](parser) -- see [`Parsers`](parser::Parsers).
//!   This flag is required by the `default-parser-ical` flag.
//!
//! # Examples
//!
//! Let's create a [`VCF`] object using the [`new()`](VCF::new()) function:
//! ```
//! use libvcf::VCF;
//! use std::any::{Any, TypeId};
//!
//! let vcf = VCF::new();
//!
//! assert!(TypeId::of::<VCF>() == vcf.type_id());
//! ```
//! Or let's create a [`VCF`] object using the builder and data contained in a [`str`]:
//!
//! ```
//! use libvcf::VCFBuilder;
//!
//! // this will lead to an error as the provided data is not valid
//! let vcf = VCFBuilder::new().from_data("NON VALID DATA").try_build();
//!
//! assert!(vcf.is_err());
//! ```
//!
//! Now let's create a [`VCF`] object using the builder and a filename:
//! ```
//! use libvcf::VCFBuilder;
//! use std::path::PathBuf;
//!
//! // this will also yield an Err as the file doesn't exist
//! let vcf = VCFBuilder::new()
//!                 .from_file(PathBuf::from("file.vcf"))
//!                 .try_build();
//!
//! assert!(vcf.is_err());
//! ```
//!
//! It is also possible to use the builder with a [`Vec`] containing various filenames:
//! ```
//! use libvcf::VCFBuilder;
//! use std::path::PathBuf;
//!
//! let filenames = vec![
//!     PathBuf::from("file.vcf"),
//!     PathBuf::from("file2.vcf"),
//!     PathBuf::from("file3.vcf")
//! ];
//!
//! // this will yield an Err as the files don't exist
//! let vcf = VCFBuilder::new()
//!                 .from_files(filenames)
//!                 .try_build();
//!
//! assert!(vcf.is_err());
//! ```
//!
#![warn(missing_docs)]
mod dbg;
pub mod format_detector;
#[macro_use]
mod macros;
pub mod parser;
pub mod stats;
mod ui;
mod vcard;
mod vcardproperty;
pub mod vcf;

/// This crate has to be reexported as it is used by the vcf! macro
pub extern crate paste;
pub use crate::vcard::Vcard;
pub use crate::vcardproperty::VCardProperty;
use std::path::PathBuf;

/// The [`VCF`] struct, which contains the VCARD entries.
///
/// It holds the vcf data and additional statistics related to its processing history.
#[derive(Clone, Default)]
pub struct VCF {
    /// entries: the actual data stored ad VCard objects (from the vcard crate)
    entries: Vec<Vcard>,
}

/// A builder interface for the [`VCF`] struct.
///
#[derive(Default)]
pub struct VCFBuilder {
    /// split: if true, split the input data before parsing
    split: bool,
    /// ignore_errors: if true, ignore all parsing errors
    ignore_errors: bool,
    /// file_names: an optional Vector of file paths to get the data from
    file_names: Option<Vec<PathBuf>>,
    /// data: an optional String containing the vcf data
    data: Option<String>,
}
