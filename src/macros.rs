//! This module contains macros for easily creating an instance of different types
//! of the crate.

/// Creates a `String` containing the corresponding vcf formatted data
#[macro_export]
macro_rules! vcf_string {
    (@expand HEADER ) => {
        format!("BEGIN:VCARD\r\n")
    };
    (@expand FOOTER ) => {
        format!("END:VCARD\r\n")
    };
    (@expand $name:literal: [$value:literal] ) => {
        format!("{}:{}\r\n", $name, $value)
    };
    (@expand $name:literal: [$value1:literal, $( $value2:literal ),+] ) => {
        format!("{}:{}\r\n{}",
            $name,
            $value1,
            vcf_string!(@expand $name: [$( $value2 ),+])
        )
    };
    (@expand $name:literal: [$( $value1:literal ),+], $($name2:literal: [$( $value2:literal ),+]),+ ) => {
        format!("{}{}",
            vcf_string!(@expand $name: [$( $value1 ),+]),
            vcf_string!(@expand $($name2: [$( $value2 ),+]),+)
        )
    };
    ( $( [ $( $string:literal: [$( $value:literal ),+] ),+ ] ),+ ) => {
        {
            let mut result = String::new();
            $(
                result.push_str(
                    format!("{}{}{}",
                        vcf_string!(@expand HEADER),
                        vcf_string!(@expand $( $string: [$( $value ),+] ),+ ),
                        vcf_string!(@expand FOOTER)
                    ).as_str()
                );
            )+
            result
        }
    };
}

/// Creates a VCF object
///
/// Usage:
/// ```
/// use libvcf::VCF;
/// use libvcf::vcf;
/// use std::any::{Any, TypeId};
///
/// let vcf = vcf!(
///     [
///         "N": [ "Test;;;;" ],
///         "TEL": [ "0123456789" ],
///         "EMAIL": [ "test@example.com", "alias1@example.com", "alias2@example.com" ]
///     ],
///     [
///         "FN": [ "Another person" ],
///         "TEL": [ "0987654321" ],
///         "EMAIL": [ "test@example2.com", "alias1@example2.com", "alias2@example2.com" ]
///     ]
/// );
///
/// assert_eq!(TypeId::of::<VCF>(), vcf.type_id());
/// ```
#[macro_export]
macro_rules! vcf {
    (@expand HEADER ) => {
        format!("BEGIN:VCARD\r\n")
    };
    (@expand FOOTER ) => {
        format!("END:VCARD\r\n")
    };
    (@expand $name:literal: [$value:literal] ) => {
        format!("{}:{}\r\n", $name, $value)
    };
    (@expand $name:literal: [$value1:literal, $( $value2:literal ),+] ) => {
        format!("{}:{}\r\n{}",
            $name,
            $value1,
            vcf!(@expand $name: [$( $value2 ),+])
        )
    };
    (@expand $name:literal: [$( $value1:literal ),+], $($name2:literal: [$( $value2:literal ),+]),+ ) => {
        format!("{}{}",
            vcf!(@expand $name: [$( $value1 ),+]),
            vcf!(@expand $($name2: [$( $value2 ),+]),+)
        )
    };
    (@str_parse $( [ $( $string:literal: [$( $value:literal ),+] ),+ ] ),+ ) => {
        {
            let mut result = String::new();
            $(
                result.push_str(
                    format!("{}{}{}",
                        vcf!(@expand HEADER),
                        vcf!(@expand $( $string: [$( $value ),+] ),+ ),
                        vcf!(@expand FOOTER)
                    ).as_str()
                );
            )+
            result
        }
    };
    // Entry point when building without stats
    ( $( [ $( $name:literal: [ $( $value:literal ),+ ] ),+ ] ),+ ) => {
        {
            use std::str::FromStr;
            let mut vcf = $crate::VCF::from_str(vcf!(@str_parse $( [ $( $name: [ $( $value ),+ ] ),+ ] ),+ ).as_str()).expect("BUG: Input is not valid");
            vcf.sort();
            vcf
        }
    };
}

#[cfg(test)]
mod tests {

    #[test]
    fn vcf_string_one_vcard() {
        let result = vcf_string!([ "FN": ["Test"] ]);
        let expect = "\
BEGIN:VCARD\r\n\
FN:Test\r\n\
END:VCARD\r\n\
";
        assert_eq!(result, expect);

        let result = vcf_string!([ "EMAIL": ["test@example.com", "test2@example.com"] ]);
        let expect = "\
BEGIN:VCARD\r\n\
EMAIL:test@example.com\r\n\
EMAIL:test2@example.com\r\n\
END:VCARD\r\n\
";
        assert_eq!(result, expect);

        let result = vcf_string!(
            [
                "FN": ["Test"],
                "N": ["Test;;;;"],
                "TEL": ["0123456789", "0213456789"],
                "EMAIL": ["test@example.com", "test2@example.com"]
            ]
        );
        let expect = "\
BEGIN:VCARD\r\n\
FN:Test\r\n\
N:Test;;;;\r\n\
TEL:0123456789\r\n\
TEL:0213456789\r\n\
EMAIL:test@example.com\r\n\
EMAIL:test2@example.com\r\n\
END:VCARD\r\n\
";
        assert_eq!(result, expect);

        let result = vcf_string!(
            [
                "FN": ["Test"],
                "N": ["Test;;;;"],
                "TEL": ["0123456789"],
                "EMAIL": ["test@example.com"]
            ]
        );
        let expect = "\
BEGIN:VCARD\r\n\
FN:Test\r\n\
N:Test;;;;\r\n\
TEL:0123456789\r\n\
EMAIL:test@example.com\r\n\
END:VCARD\r\n\
";
        assert_eq!(result, expect);

        let result = vcf_string!(
            [
                "FN": ["Test"],
                "N": ["Test;;;;"],
                "TEL": ["0123456789", "0213456789", "0321456789"],
                "EMAIL": ["test@example.com", "test2@example.com", "test3@example.com"]
            ]
        );
        let expect = "\
BEGIN:VCARD\r\n\
FN:Test\r\n\
N:Test;;;;\r\n\
TEL:0123456789\r\n\
TEL:0213456789\r\n\
TEL:0321456789\r\n\
EMAIL:test@example.com\r\n\
EMAIL:test2@example.com\r\n\
EMAIL:test3@example.com\r\n\
END:VCARD\r\n\
";
        assert_eq!(result, expect);

        let result = vcf_string!(
            [
                "FN": ["Test"],
                "N": ["Test;;;;"],
                "TEL": ["0123456789", "0213456789"],
                "EMAIL": ["test@example.com", "test2@example.com", "test3@example.com"]
            ]
        );
        let expect = "\
BEGIN:VCARD\r\n\
FN:Test\r\n\
N:Test;;;;\r\n\
TEL:0123456789\r\n\
TEL:0213456789\r\n\
EMAIL:test@example.com\r\n\
EMAIL:test2@example.com\r\n\
EMAIL:test3@example.com\r\n\
END:VCARD\r\n\
";
        assert_eq!(result, expect);
    }

    #[test]
    fn vcf_string_two_vcards() {
        let result = vcf_string!(
            [
                "FN": ["Test"],
                "N": ["Test;;;;"],
                "TEL": ["0123456789", "0213456789"],
                "EMAIL": ["test@example.com", "test2@example.com", "test3@example.com"]
            ],
            [
                "FN": ["Test2"],
                "N": ["Test2;;;;"],
                "TEL": ["0123456789", "0213456789"],
                "EMAIL": ["test@example.com", "test2@example.com", "test3@example.com"]
            ]
        );
        let expect = "\
BEGIN:VCARD\r\n\
FN:Test\r\n\
N:Test;;;;\r\n\
TEL:0123456789\r\n\
TEL:0213456789\r\n\
EMAIL:test@example.com\r\n\
EMAIL:test2@example.com\r\n\
EMAIL:test3@example.com\r\n\
END:VCARD\r\n\
BEGIN:VCARD\r\n\
FN:Test2\r\n\
N:Test2;;;;\r\n\
TEL:0123456789\r\n\
TEL:0213456789\r\n\
EMAIL:test@example.com\r\n\
EMAIL:test2@example.com\r\n\
EMAIL:test3@example.com\r\n\
END:VCARD\r\n\
";
        assert_eq!(result, expect);
    }

    #[test]
    fn vcf_no_stats() {
        let result = vcf!(
            [
                "FN": ["Test"],
                "N": ["Test;;;;"],
                "TEL": ["0123456789", "0213456789"],
                "EMAIL": ["test@example.com", "test2@example.com", "test3@example.com"]
            ],
            [
                "FN": ["Test2"],
                "N": ["Test2;;;;"],
                "TEL": ["0123456789", "0213456789"],
                "EMAIL": ["test@example.com", "test2@example.com", "test3@example.com"]
            ]
        );

        let expect = crate::VCFBuilder::new()
            .from_data(
                "\
BEGIN:VCARD\r\n\
FN:Test\r\n\
N:Test;;;;\r\n\
TEL:0123456789\r\n\
TEL:0213456789\r\n\
EMAIL:test@example.com\r\n\
EMAIL:test2@example.com\r\n\
EMAIL:test3@example.com\r\n\
END:VCARD\r\n\
BEGIN:VCARD\r\n\
FN:Test2\r\n\
N:Test2;;;;\r\n\
TEL:0123456789\r\n\
TEL:0213456789\r\n\
EMAIL:test@example.com\r\n\
EMAIL:test2@example.com\r\n\
EMAIL:test3@example.com\r\n\
END:VCARD\r\n",
            )
            .try_build()
            .expect("BUG: input should not produce parsing error");

        assert_eq!(result, expect);
    }

    #[test]
    fn vcf_macro_3entries() {
        let result = vcf!(
            [
                "FN": ["Test"],
                "N": ["Test;;;;"],
                "TEL": ["0123456789", "0213456789"],
                "EMAIL": ["test@example.com", "test2@example.com", "test3@example.com"]
            ],
            [
                "FN": ["Test2"],
                "N": ["Test2;;;;"],
                "TEL": ["0123456789", "0213456789"],
                "EMAIL": ["test@example.com", "test2@example.com", "test3@example.com"]
            ],
            [
                "FN": ["Test3"],
                "N": ["Test3;;;;"],
                "TEL": ["0123456789", "0213456789"],
                "EMAIL": ["test@example.com", "test2@example.com", "test3@example.com"]
            ]
        );

        let expect = crate::VCFBuilder::new()
            .from_data(
                "\
BEGIN:VCARD\r\n\
FN:Test\r\n\
N:Test;;;;\r\n\
TEL:0123456789\r\n\
TEL:0213456789\r\n\
EMAIL:test@example.com\r\n\
EMAIL:test2@example.com\r\n\
EMAIL:test3@example.com\r\n\
END:VCARD\r\n\
BEGIN:VCARD\r\n\
FN:Test2\r\n\
N:Test2;;;;\r\n\
TEL:0123456789\r\n\
TEL:0213456789\r\n\
EMAIL:test@example.com\r\n\
EMAIL:test2@example.com\r\n\
EMAIL:test3@example.com\r\n\
END:VCARD\r\n
BEGIN:VCARD\r\n\
FN:Test3\r\n\
N:Test3;;;;\r\n\
TEL:0123456789\r\n\
TEL:0213456789\r\n\
EMAIL:test@example.com\r\n\
EMAIL:test2@example.com\r\n\
EMAIL:test3@example.com\r\n\
END:VCARD\r\n",
            )
            .try_build()
            .expect("BUG: input should not produce parsing error");

        assert_eq!(result, expect);
    }
}
