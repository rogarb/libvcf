//! This module contains the bits related to the [`Stats`] structure.
//!
//! This object contains different metrics related to a given [`VCF`] instance.
//!
use crate::VCardProperty;
use crate::VCF;
use std::collections::HashMap;
use std::time;

#[derive(Clone, Debug)]
/// The [`Stats`] struct related to a given [`VCF`] instance.
pub struct Stats {
    /// Number of entries in the [`VCF`] instance.
    total_entries: usize,
    /// Creation time of the instance. This allows to compute elapsed times.
    creation_time: time::Instant,
    /// Count of duplicate per field name ("N", "FN",...)
    duplicates_per_field: HashMap<String, usize>,
    /// Count of values per field name ("N", "FN",...)
    count_per_field: HashMap<String, usize>,
}

impl Default for Stats {
    fn default() -> Self {
        Stats {
            total_entries: 0,
            creation_time: time::Instant::now(),
            duplicates_per_field: HashMap::default(),
            count_per_field: HashMap::default(),
        }
    }
}

impl PartialEq for Stats {
    /// This implementation of PartialEq ignores the creation_time field when comparing two
    /// instances of [`Stats`].
    fn eq(&self, other: &Self) -> bool {
        self.total_entries.eq(&other.total_entries)
            && self.count_per_field.eq(&other.count_per_field)
            && self.duplicates_per_field.eq(&other.duplicates_per_field)
    }
}

impl Eq for Stats {}

impl From<&VCF> for Stats {
    /// Create an instance from a [`VCF`] object
    fn from(vcf: &VCF) -> Self {
        let mut stats = Self::new();

        let mut hm: HashMap<String, HashMap<String, usize>> = HashMap::new();
        // counts how many times each value of each field appears
        for entry in vcf.entries() {
            for (name, properties) in entry.name_properties() {
                for property in properties {
                    *hm.entry(name.to_owned())
                        .or_insert_with(HashMap::new)
                        .entry(property.value_as_string())
                        .or_insert(0) += 1;
                }
            }
        }
        for (key, value) in hm.iter() {
            *stats
                .duplicates_per_field
                .entry(key.to_owned())
                .or_insert(0) += value.values().filter(|&&v| v > 1).count();
            *stats.count_per_field.entry(key.to_owned()).or_insert(0) +=
                value.values().sum::<usize>();
        }
        stats.total_entries = vcf.len();
        stats
    }
}

impl Stats {
    /// Creates a new instance of [`Stats`].
    pub fn new() -> Self {
        Self::default()
    }

    /// Returns the value stored in the total entries counter.
    pub fn get_total_entries(&self) -> usize {
        self.total_entries
    }

    /// Returns the count of duplicate values given a VCardProperty
    pub fn get_duplicate_values_by_property(&self, prop: VCardProperty) -> usize {
        *self
            .duplicates_per_field
            .get(&prop.to_string())
            .unwrap_or(&0)
    }

    /// Returns the count of duplicate email values.
    pub fn get_duplicate_emails(&self) -> usize {
        *self.duplicates_per_field.get("EMAIL").unwrap_or(&0)
    }

    /// Returns the count of duplicate phone values.
    pub fn get_duplicate_phones(&self) -> usize {
        *self.duplicates_per_field.get("TEL").unwrap_or(&0)
    }

    /// Returns the count of duplicate name values.
    pub fn get_duplicate_names(&self) -> usize {
        *self.duplicates_per_field.get("N").unwrap_or(&0)
            + *self.duplicates_per_field.get("FN").unwrap_or(&0)
    }

    /// Returns a String containing a summary of the differences between two Stats
    /// instances.
    ///
    /// This function ignores the creation_time field.
    pub fn diff(&self, other: &Self) -> String {
        let mut output = String::new();

        macro_rules! append_diff_str_from_method {
            ( $method:ident, $str:literal, $output:ident ) => {
                let diff_var = self.$method().abs_diff(other.$method());
                if diff_var != 0 {
                    $output.push_str(
                        format!(
                            "{} {} by {diff_var} (new value: {})\n",
                            $str,
                            if self.$method() > other.$method() {
                                "decreased"
                            } else {
                                "increased"
                            },
                            other.$method()
                        )
                        .as_str(),
                    );
                }
            };
        }

        append_diff_str_from_method!(get_total_entries, "Total entries", output);
        append_diff_str_from_method!(get_duplicate_names, "Duplicate name field count", output);
        append_diff_str_from_method!(get_duplicate_phones, "Duplicate phone field count", output);
        append_diff_str_from_method!(get_duplicate_emails, "Duplicate email field count", output);

        macro_rules! append_diff_str_from_hm {
            ( $hm:ident, $str:literal, $output:ident ) => {
                $output.push_str(format!(" - {}:\n", $str).as_str());
                let mut keys = self
                    .$hm
                    .keys()
                    .chain(other.$hm.keys())
                    .map(|k| k.to_owned())
                    .collect::<Vec<String>>();
                keys.sort();
                keys.dedup();
                for key in keys {
                    let old = *self.$hm.get(&key).unwrap_or(&0);
                    let new = *other.$hm.get(&key).unwrap_or(&0);
                    let diff = old.abs_diff(new);
                    if diff != 0 {
                        $output.push_str(
                            format!(
                                "   * field {} {} by {diff} (new value: {})\n",
                                key,
                                if old > new { "decreased" } else { "increased" },
                                new
                            )
                            .as_str(),
                        );
                    }
                }
            };
        }

        append_diff_str_from_hm!(count_per_field, "values per field", output);
        append_diff_str_from_hm!(duplicates_per_field, "duplicates per field", output);

        if output.is_empty() {
            output.push_str("None");
        }
        output
    }

    /// Returns a String containing a summary of the differences between the current
    /// stats and the stats of the given [`VCF`] instance.
    ///
    /// See [`Stats::diff()`] for details.
    pub fn diff_with(&self, vcf: &VCF) -> String {
        self.diff(&Stats::from(vcf))
    }

    /// Returns the elapsed time since the creation of the instance.
    pub fn elapsed_time(&self) -> time::Duration {
        self.creation_time.elapsed()
    }
}

impl std::fmt::Display for Stats {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut output = String::from(" - entry per field count:\n");
        for (name, count) in self.count_per_field.iter() {
            output.push_str(format!("\t * field \"{}\": {}\n", name, count).as_str());
        }
        output.push_str(" - duplicate entry count:\n");
        for (name, count) in self.duplicates_per_field.iter() {
            output.push_str(format!("\t * field \"{}\": {}\n", name, count).as_str());
        }
        write!(
            f,
            "\
Total entries: {}
Duplicate fields:
    - names: {}
    - emails: {}
    - phones: {}
Elapsed time since creation: {:#?}
Extended stats:
{}",
            self.get_total_entries(),
            self.get_duplicate_names(),
            self.get_duplicate_emails(),
            self.get_duplicate_phones(),
            self.elapsed_time(),
            output
        )
    }
}

#[cfg(test)]
mod tests {
    /// Creates a new instance of ExtendedStats structure.
    ///
    /// # Arguments
    /// $field is a str
    /// $count and $dup_count are usize integers
    ///
    macro_rules! extended_stats {
        ( total: $total:literal, $( $field:literal: $count:literal, $dup_count:literal ),+ ) => {
            {
                let mut count_per_field = std::collections::HashMap::new();
                let mut duplicates_per_field = std::collections::HashMap::new();
                $(
                    *count_per_field.entry($field.to_string()).or_insert(0) += $count;
                    *duplicates_per_field.entry($field.to_string()).or_insert(0) += $dup_count;
                )+
                $crate::stats::Stats {
                    total_entries: $total,
                    duplicates_per_field,
                    count_per_field,
                    ..$crate::stats::Stats::default()
                }
            }
        }
    }

    #[test]
    fn macro_extended_stats() {
        use super::Stats;
        use std::collections::HashMap;

        let result = extended_stats!(
            total: 25,
            "N": 3, 1,
            "FN": 5, 2,
            "PATXI": 34, 23
        );

        let mut duplicates_per_field = HashMap::new();
        let mut count_per_field = HashMap::new();
        count_per_field.insert("N".to_string(), 3);
        count_per_field.insert("FN".to_string(), 5);
        count_per_field.insert("PATXI".to_string(), 34);
        duplicates_per_field.insert("N".to_string(), 1);
        duplicates_per_field.insert("FN".to_string(), 2);
        duplicates_per_field.insert("PATXI".to_string(), 23);
        let expected = Stats {
            total_entries: 25,
            duplicates_per_field,
            count_per_field,
            ..crate::stats::Stats::default()
        };

        assert_eq!(result, expected);
    }

    #[test]
    fn from() {
        let result = super::Stats::from(&vcf!(
            [
                "FN": ["Test"],
                "N": ["Test;;;;"],
                "TEL": ["0123456789"],
                "EMAIL": ["test@example.com", "test2@example.com"]
            ],
            [
                "FN": ["Test2"],
                "N": ["Test2;;;;"],
                "TEL": ["0123456789", "0213456789"],
                "EMAIL": ["test@example.com", "test2@example.com", "test3@example.com"]
            ],
            [
                "FN": ["Test3"],
                "N": ["Test3;;;;"],
                "TEL": ["0213456789"],
                "EMAIL": ["test@example.com", "test3@example.com"]
            ]
        ));
        let expected = extended_stats!(
            total: 3,
            "N": 3, 0,
            "FN": 3, 0,
            "TEL": 4, 2,
            "EMAIL": 7, 3
        );
        assert_eq!(result, expected);
    }
}
