//! This module contains user interface implementation
//!
use std::io;

/// Asks for user confirmation with a message, returnin a `bool`
pub fn ask_user_confirmation(msg: &str) -> bool {
    ask_user_confirmation_helper(io::stdin().lock(), io::stderr(), msg)
}

/// Helper for ask_user_confirmation() function which functionality can be easily tested
fn ask_user_confirmation_helper<R, W>(mut reader: R, mut writer: W, msg: &str) -> bool
where
    R: io::BufRead,
    W: io::Write,
{
    let mut answer = String::new();
    if write!(&mut writer, "{} (Yes/No): ", msg).is_ok() {
        reader.read_line(&mut answer).is_ok()
            && vec!["y", "yes"].iter().any(|&e| e == answer.to_lowercase())
    } else {
        false
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn ask_user_confirmation_helper() {
        use super::ask_user_confirmation_helper;

        let msg = "Test msg";
        let mut output = Vec::new();
        let input = String::from("Y");
        let expected = format!("{} (Yes/No): ", msg);

        assert!(ask_user_confirmation_helper(
            &mut input.as_bytes(),
            &mut output,
            msg
        ));
        let output = String::from_utf8(output).unwrap();
        assert_eq!(output, expected);

        let input = String::from("Yes");
        let mut output = Vec::new();
        assert!(ask_user_confirmation_helper(
            &mut input.as_bytes(),
            &mut output,
            msg
        ));

        let input = String::from("random");
        assert!(!ask_user_confirmation_helper(
            &mut input.as_bytes(),
            &mut output,
            msg
        ));
    }
}
